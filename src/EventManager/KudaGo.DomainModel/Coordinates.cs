﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKE.KudaGo.Domain.Model
{
    public class Coordinates
    {
        [JsonProperty("lat")]
        [Column(TypeName = "decimal(18, 15)")]
        public decimal? Lat { get; set; }
        [JsonProperty("lon")]
        [Column(TypeName = "decimal(18, 15)")]
        public decimal? Lon { get; set; }
    }
}
