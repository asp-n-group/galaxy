﻿using AutoMapper;
using EMD = SKE.EventManager.Domain;
using KGD = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности категорий мест.
    /// </summary>
    public class CategoryPlaceMappings : Profile
    {
        public CategoryPlaceMappings()
        {
            CreateMap<KGD.CategoryPlace, EMD.CategoryPlace>()
                .ForMember(d => d.ExternalId, map => map.MapFrom(s => s.Id))
                .ForMember(d => d.Id, map => map.Ignore());
            CreateMap<EMD.CategoryPlace, KGD.CategoryPlace>()
                .ForMember(d => d.Id, map => map.MapFrom(s => s.ExternalId))
                .ForMember(d => d.Id, map => map.Ignore());
        }
    }
}
