﻿using Identity.Data;
using Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;
using IdentityServer4.Extensions;
using System.Globalization;
using SKE.Core.RabbitMQProducer;
using SKE.EmailNotificator.Domain;

namespace Identity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly AuthDbContext _dbContext;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly IValidator<RegisterModel> _registerModelValidator;
        private readonly IValidator<UpdateModel> _updateModelValidator;
        private readonly IMqService<object> _mqService;

        public AuthController(
            UserManager<AppUser> userManager,
            AuthDbContext dbContext,
            IConfiguration configuration,
            IMapper mapper,
            IValidator<RegisterModel> registerModelValidator,
            IValidator<UpdateModel  > updateModelValidator,
            IMqService<object> mqService)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _configuration = configuration;
            _mapper = mapper;
            _registerModelValidator = registerModelValidator;
            _updateModelValidator = updateModelValidator;
            _mqService = mqService;
        }

        /// <summary>
        /// Логин
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpPost("login")]
        [SwaggerOperation("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                string firstName = string.Empty;
                string lastName = string.Empty;
                string gender = string.Empty;

                if (user.FirstName != null)
                    firstName = user.FirstName;
                
                if (user.LastName != null)  
                    lastName = user.LastName;

                if (user.Gender != null)
                    gender = user.Gender;

                var claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim(JwtRegisteredClaimNames.GivenName, firstName),
                    new Claim(JwtRegisteredClaimNames.FamilyName, lastName),
                    new Claim("Gender", gender)
                };

                if (user.Pref != null)
                {
                    foreach (var p in user.Pref)
                    {
                        if (p.Preference != null)
                            claims.Add(new Claim("Pref", p.Preference));
                    }
                }

                var jwtKey = _configuration["Jwt:Key"] ?? throw new Exception("Отсутствует ключ шифрования!");
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
                var token = new JwtSecurityToken(
                    _configuration["Jwt:Issuer"],
                    _configuration["Jwt:Audience"],
                    claims,
                    expires: DateTime.UtcNow.AddHours(1),
                    signingCredentials: credentials);

                var encodedToken = new JwtSecurityTokenHandler().WriteToken(token);

                // удалить
                // Для сваггера
                if (IsDevelopmentEnvironment())
                {
                    HttpContext.Items["AuthorizationToken"] = encodedToken;
                    HttpContext.Response.Headers.Add("Authorization", encodedToken); // удалить?
                    //HttpContext.Items["AuthorizationHeader"] = authHeader;
                }

                return Ok(new { Token = encodedToken });
            }

            return Unauthorized();
        }

        /// <summary>
        /// Регистрация нового пользователя, если при регистрации указывать интерес, которого еще нет - он будет добавлен в БД
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (model.Username.IsNullOrEmpty() && !model.Email.IsNullOrEmpty())
                model.Username = model.Email;
            
            var validationResult = await _registerModelValidator.ValidateAsync(model);

            if (!validationResult.IsValid)
            {
                // ошибка валидации
                return StatusCode(StatusCodes.Status422UnprocessableEntity, validationResult.Errors);
            }

            if (model.DateOfBirth != null)
            {
                string dateOfBirthString = model.DateOfBirth;
                string dateFormat = "dd.MM.yyyy";
                if (DateTime.TryParseExact(dateOfBirthString, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateOfBirth))
                    model.DateOfBirthParsed = dateOfBirth;
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, "Некорректный формат даты рождения!");
                }
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, "Не указана дата рождения!");

            if (model.Location == null)
            {
                model.Location = "msk";
            }

            var user = _mapper.Map<AppUser>(model);

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                if (model.Preferences != null && model.Preferences.Any())
                {
                    _dbContext.Pref.RemoveRange(_dbContext.Pref.Where(x => x.UserId == user.Id).ToList());
                    await _dbContext.SaveChangesAsync();
                    foreach (var preference in model.Preferences)
                    {
                        var userPreference = new Pref { User = user, Preference = preference };
                        _dbContext.Pref.Add(userPreference);
                    }
                    await _dbContext.SaveChangesAsync();
                }

                var message = new SendEmailRequest()
                {
                    MailTo = user.Email,
                    Message = $"{user.FirstName} {user.LastName}, Вы успешно зарегистрированы на сайте Круги и события",
                    Subject = $"Регистрация на сайте Круги и события"
                };

                _mqService.SendMessage(message, "EmailNotificationQueue");
                return Ok(new { message = "Регистрация прошла успешно." });
            }
            else
            {
                // другая ошибка при регистрации
                return StatusCode(StatusCodes.Status400BadRequest, result.Errors);
            }
        }

        /// <summary>
        /// обновление данных о пользователе
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("update")]
        [AllowAnonymous]
        public async Task<IActionResult> Update(UpdateModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user != null)
            {
                var validationResult = await _updateModelValidator.ValidateAsync(model);

                if (!validationResult.IsValid)
                {
                    // ошибка валидации
                    return StatusCode(StatusCodes.Status422UnprocessableEntity, validationResult.Errors);
                }

                if (model.FirstName != null)
                {
                    user.FirstName = model.FirstName;
                }
                
                if (model.LastName != null)
                {
                    user.LastName = model.LastName;
                }

                if (model.Location != null)
                {
                    user.Location = model.Location;
                }

                if (model.DateOfBirth != null)
                {
                    string dateOfBirthString = model.DateOfBirth;
                    string dateFormat = "dd.MM.yyyy";
                    if (DateTime.TryParseExact(dateOfBirthString, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateOfBirth))
                    {
                        user.DateOfBirthParsed = dateOfBirth;
                    }
                    else
                    {
                        return StatusCode(StatusCodes.Status400BadRequest, "Некорректный формат даты рождения!");
                    }
                }

                _dbContext.Pref.RemoveRange(_dbContext.Pref.Where(x => x.UserId == user.Id).ToList());
                await _dbContext.SaveChangesAsync();

                if (model.Preferences != null && model.Preferences.Any())
                {
                    foreach (var preference in model.Preferences)
                    {
                        var userPreference = new Pref { User = user, Preference = preference };
                        _dbContext.Pref.Add(userPreference);
                    }
                    await _dbContext.SaveChangesAsync();
                }
                return Ok(new { message = "Обновление успешно." });
            }
            return Ok(new { message = "Пользователь не найден." });
        }

        /// <summary>
        /// Логаут
        /// </summary>
        /// <returns></returns>
        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            
            // удалить
            // Для сваггера
            //if (IsDevelopmentEnvironment())
            //{
            //    HttpContext.Response.Headers.Remove("Authorization");
            //}
            return Ok(new { message = "Вы успешно вышли из системы." });
        }

        /// <summary>
        /// Проверить, авторизован ли пользователь в текущий момент
        /// </summary>
        /// <returns></returns>
        [HttpPost("is-authorized")]
        [AllowAnonymous]
        public IActionResult IsAuthorized()
        {
            if (User.Identity != null)
            {
                if (User.Identity.IsAuthenticated)
                {
                    var userName = User.Identity.Name;
                    // Дополнительные действия, если пользователь авторизован
                    return Ok("Пользователь авторизован");
                }
            }

            return Ok("Пользователь не авторизован");
        }

        private bool IsDevelopmentEnvironment()
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            return environment == "Development";
        }
    }
}