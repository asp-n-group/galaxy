﻿namespace SKE.Feed.Services.Abstractions
{
    public interface IUserInterestsService
    {
        public List<string> GetUserInterests(int userId);
        public string CorrectInterest(string interest);
    }
}
