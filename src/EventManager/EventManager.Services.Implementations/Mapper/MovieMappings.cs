﻿using AutoMapper;
using EMD = SKE.EventManager.Domain;
using KGD = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности Фильм.
    /// </summary>
    public class MovieMappings : Profile
    {
        public MovieMappings()
        {
            CreateMap<KGD.Movie, EMD.Movie>()
                .ForMember(d => d.ExternalId, map => map.MapFrom(s => s.Id))
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Genres, map => map.Ignore());
        }
    }
}
