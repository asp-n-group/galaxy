import { StyledEngineProvider } from '@mui/material/styles';
import SkeAppBar from './Components/SkeAppBar';
import SkeFooter from './Components/SkeFooter';
import { Container, ListItem, Paper, Stack, Typography } from '@mui/material';
import { useState, useEffect } from 'react';
import Unregistred from './Pages/Unregistred'
import RegistredPage from './Pages/Registred'
import Box from '@mui/material/Box'; 
import Grid from '@mui/material/Grid';
import EventItemDetails from './Pages/EventItemDetails';
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Registration from './Pages/Registration';
import Login from './Pages/Login';
import Profile from './Pages/Profile';
import Registred from './Pages/Registred';
import Tags from './Components/Tags';
import UseToken from './UseToken'; 
import CreateCircle from './Pages/CreateCircle'; 



export function App() {
  const [selectedLocation, selectLocation ] = useState('');
  const [selectedCat, setCat ] = useState([]);

  const ShowTags = (event) => { 

    if(UseToken().token !== undefined) 
    {
      return (<Tags  />)
    }
  };

  return (
    <Box>
      <Paper sx={{ position: 'fixed', display:'block', zIndex:1234, width:'100%', top: 0, left: 0}} >
        <SkeAppBar selectedLocation={selectedLocation} selectLocation={selectLocation}/> 
      </Paper>
      <Stack sx={{ borderLeft:50, pt:10, pb:10, borderRight:50, alignContent:'center', 
            justifyContent:'center', borderColor:'white'}}  
          direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={1}>
        <Router key={0}>
          <Routes key={1}>
            <Route key={2} path="/" element={<Unregistred selectedLocation={selectedLocation} selectLocation={selectLocation} selectedCat={selectedCat} setCat={setCat}/>}/>
            <Route key={3} path="/registred" element={<Registred selectedLocation={selectedLocation} selectLocation={selectLocation} selectedCat={selectedCat} setCat={setCat}/>}/>
            <Route key={4} path="/create" element={<CreateCircle/>}/>
            <Route key={5} path="/registration" element={<Registration/>}/>
            <Route key={6} path="/login" element={<Login/>}/>
            <Route key={7} path="/profile" element={<Profile/>}/>
            <Route key={8} path="/details/:id" element={<EventItemDetails/>}  /> 
          </Routes>
        </Router>
      </Stack>
      <Paper sx={{ position: 'fixed', width:'100%', bottom: 0, left: 0, height:'6%'}} >
        <SkeFooter  />
      </Paper>
    </Box> 
  );
}
 
export default App;