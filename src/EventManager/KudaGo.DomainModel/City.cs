﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class City
    {
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("timezone")]
        public string Timezone { get; set; }
        [JsonProperty("coords")]
        public Coordinates Coords { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("currency")]
        public string Currency { get; set; }
    }
}
