﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Связь многие ко многим теги и места
    /// </summary>
    public class TagToPlace : BaseEntity
    {
        /// <summary>
        /// идентификатор тега 
        /// </summary>
        public Guid TagId { get; set; }
        /// <summary>
        /// тег
        /// </summary>
        public Tag Tag { get; set; }

        /// <summary>
        /// идентификатор места
        /// </summary>
        public Guid PlaceId { get; set; }
        /// <summary>
        /// место
        /// </summary>
        public Place Place { get; set; }
    }
}
