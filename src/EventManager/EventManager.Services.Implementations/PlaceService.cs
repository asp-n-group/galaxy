﻿using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Contracts;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Services.Implementations
{
    public class PlaceService : IPlaceService
    {
        IPlaceRepository _places;
        public PlaceService(IPlaceRepository places)
        {
            _places = places;
        }

        public async Task<IEnumerable<Place>> GetPlaces(PlaceFilterDTO placeFilterDTO)
        {
            return await _places.GetFitleredAsync(placeFilterDTO);
        }
    }
}
