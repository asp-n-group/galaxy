﻿using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Services.Abstractions
{
    public interface IMovieService
    {
        Task<IEnumerable<Movie>> GetMovies(MovieFilterDTO movieFilterDTO);
    }
}
