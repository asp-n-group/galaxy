﻿
using SKE.Core.Repositories.Abstractions;
using SKE.EventManager.Domain;

namespace SKE.EventManager.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы c жанрами
    /// </summary>
    public interface IGenreRepository : IRepository<Genre, Guid>
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <returns> Список жанров. </returns>
        Task<List<Genre>> GetFitleredAsync();
    }
}
