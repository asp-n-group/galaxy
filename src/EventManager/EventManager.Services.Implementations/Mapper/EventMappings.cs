﻿using AutoMapper;
using EMD = SKE.EventManager.Domain;
using KGD = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности событие.
    /// </summary>
    public class EventMappings : Profile
    {
        public EventMappings()
        {
            CreateMap<KGD.Event, EMD.Event>()
                .ForMember(d => d.ExternalId, map => map.MapFrom(s => s.Id))
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Location, map => map.Ignore())
                .ForMember(d => d.LocationId, map => map.Ignore())
                .ForMember(d => d.Place, map => map.Ignore())
                .ForMember(d => d.PlaceId, map => map.Ignore())
                .ForMember(d => d.Categories, map => map.Ignore())
                .ForMember(d => d.Tags, map => map.Ignore());
        }
    }
}
