import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import { StyledEngineProvider } from '@mui/material/styles';
import { Container, ListItem, Paper, Typography } from '@mui/material';
import EventItem from '../Components/EventItem'
import Box from '@mui/material/Box'; 
import Grid from '@mui/material/Grid';
import FeedService from '../Services/FeedService';
import { useState, useEffect } from 'react';
import Tags from '../Components/Tags';
import Button from '@mui/material/Button';

function Unregistred(props) {
  const [sizeCat, setSizeCat ] = useState(0);
  const [selectLoc, selectL] = useState(props.selectedLocation);
  const [eventsDatas, setEventsDatas] = useState([]);
  const [loaded, setLoaded] = useState(false);

  const [page, setPage] = useState(0);
  const [pageSize, setSize] = useState(20);
  const [disableNextButton, setNextButton] = useState(false);


  useEffect(() => {
    if(loaded)
    {
      return;
    }
    setLoaded(true);
    datap();    
  },[eventsDatas, loaded]);

  const datap = async ()=>{
    var d = []
    if(page === 0)
    {
      d = (await FeedService.getData(undefined, props.selectedLocation, page, pageSize,[],props.selectedCat)).events;
      setNextButton(false);
    }
    else
    {
      d = Array.from(eventsDatas);
      var newData = (await FeedService.getData(undefined, props.selectedLocation, page, pageSize,[],props.selectedCat)).events;
      d = d.concat(newData);
      if(newData.length !== pageSize)
      {
        setNextButton(true);
      }
    }
    setEventsDatas(d);
  };

  const checkState = () => {
    var stateChange = false;
    if(selectLoc !== props.selectedLocation)
    {
      selectL(props.selectedLocation);
      stateChange = true;
    }
    if(sizeCat !== props.selectedCat.length)
    {
      setSizeCat(props.selectedCat.length)
      stateChange = true;
    }

    if(stateChange === true)
    {
      setPage(0);
      setLoaded(false);
    }
  }

  const setNextEvent = async () => {
    setPage(page+1);
    setLoaded(false);
  };
  
  checkState();
  return (
    <Box sx={{ p: 0}} >
      <Tags selectedCat={props.selectedCat} setCat={props.setCat}/>
      <Box sx={{ p: 1, bgcolor: 'background.default', display: 'grid',
          gridTemplateColumns: { md: '1fr 1fr 1fr 1fr' }, gap: 2, }}>
        {eventsDatas.map((itemDatas, index) => (
          <EventItem key={index} itemData={itemDatas} sindex={itemDatas.id}/>
            ))}
      </Box>
      <Box display="flex" justifyContent="center" sx={{ p: 0 ,}}>
        <Button variant="contained" color="primary" onClick={setNextEvent }  disabled={disableNextButton}>
          Показать ещё
        </Button>
      </Box>
    </Box>
  );
}

export default Unregistred;
