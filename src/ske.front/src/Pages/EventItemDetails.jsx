import * as React from 'react';
import { StyledEngineProvider } from '@mui/material/styles';
import SkeAppBar from '../Components/SkeAppBar';
import SkeFooter from '../Components/SkeFooter';
import EventItemDetailsCard from '../Components/EventItemDetailsCard';
import { Container, ListItem, Paper, Typography } from '@mui/material';
import EventItem from '../Components/EventItem'
import Box from '@mui/material/Box'; 
import Grid from '@mui/material/Grid';
import CommentBox from '../Components/CommentBox';
import FeedService from '../Services/FeedService';
import { useParams } from 'react-router';
import { useState , useEffect} from 'react';
import useToken from '../UseToken';
import { useSearchParams } from 'react-router-dom'


function EventItemDetails(props) {
  const { id } = useParams();
  const [eventData, setEventData] = useState({});
  var token = useToken().token;
  const [searchParams, setSearchParams] = useSearchParams()
  const circle = searchParams.get('circle')

  useEffect(() => {    
    const datap = async ()=>{
      var elements = [id];
      var data = await FeedService.getDataById(elements);
      setEventData(data.events[0]);
    };
    datap();
  },[id]);

    
  return (
    <div>
      <Box sx={{ bgcolor: 'background.default', display: 'grid', margin: '0',}}>
        <EventItemDetailsCard itemDetails={eventData} selectCircle={circle}/>
      </Box> 
    </div>
  );
}

 
export default EventItemDetails;
