﻿
using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Источник изображения
    /// </summary>
    public class Source : BaseEntity
    {
        /// <summary>
        /// имя источника
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// ссылка
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// идентификатор изображения
        /// </summary>
        public Guid ImageId { get; set; }
        /// <summary>
        /// изображение
        /// </summary>
        public Image? Image { get; set; }
    }
}
