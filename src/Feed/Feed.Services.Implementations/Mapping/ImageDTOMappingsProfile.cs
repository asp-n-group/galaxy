﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;

namespace SKE.Feed.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class ImageDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public ImageDTOMappingsProfile()
        {
            CreateMap<ImageGRPC, ImageDTO>()
                .ForMember(x => x.Thumbnails, y => y.Ignore());
        }
    }
}
