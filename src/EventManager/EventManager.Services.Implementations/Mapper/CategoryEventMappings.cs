﻿using AutoMapper;
using EMD = SKE.EventManager.Domain;
using KGD = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности категорий событий.
    /// </summary>
    public class CategoryEventMappings : Profile
    {
        public CategoryEventMappings()
        {
            CreateMap<KGD.CategoryEvent, EMD.CategoryEvent>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.ExternalId, map => map.MapFrom(s => s.Id));
            CreateMap<EMD.CategoryEvent, KGD.CategoryEvent>()
                .ForMember(d => d.Id, map => map.MapFrom(s => s.ExternalId))
                .ForMember(d => d.Id, map => map.Ignore());
        }
    }
}
