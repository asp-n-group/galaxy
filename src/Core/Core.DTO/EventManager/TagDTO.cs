﻿namespace SKE.Core.DTO.EventManager
{
    public class TagDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
