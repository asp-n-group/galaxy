﻿using Newtonsoft.Json;
using Org.BouncyCastle.Asn1.Ocsp;
using SKE.CoreLib.Core;
using SKE.EmailNotificator.Domain;
using SKE.EmailNotifiicator.Services.Repositories.Abstractions;

namespace SKE.EmailNotificator.Service
{
    public class EmailNotificatorQueueConsumerService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly Consumer _consumer;

        public EmailNotificatorQueueConsumerService(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _serviceProvider = serviceProvider;
            _consumer = new Consumer(
                    configuration["RabbitMQ:Host"],
                    configuration["RabbitMQ:User"],
                    configuration["RabbitMQ:Password"],
                    "EmailNotificationQueue");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _consumer.Dispose();
                return Task.CompletedTask;
            }
            _consumer.ReceiveMessage(HandleMessage);

            return Task.CompletedTask;
        }

        private async void HandleMessage(string message)
        {
            var sendEmailRequest = JsonConvert.DeserializeObject<SendEmailRequest>(message);

            if (sendEmailRequest != null)
            {
                using var scope = _serviceProvider.CreateScope();
                var service = scope.ServiceProvider.GetRequiredService<IEmailSendingService>();
                await service.SendEmailAsync(sendEmailRequest);
            }
        }
    }
}
