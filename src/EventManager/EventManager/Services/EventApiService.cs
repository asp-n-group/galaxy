﻿using AutoMapper;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using SKE.Core.gRPCLib;
using SKE.EventManager.Controllers;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Contracts;
using System.Text.Json;

namespace SKE.EventManager.Services
{
    /// <summary>
    /// Сервис получения событий по gRPC
    /// </summary>
    public class EventApiService : EventGRPC.EventGRPCBase
    {
        private readonly IEventService _service;
        private readonly IMapper _mapper;
        private readonly ILogger<DictionaryController> _logger;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="service"></param>
        /// <param name="mapper"></param>
        /// <param name="logger"></param>
        public EventApiService(IEventService service,
            IMapper mapper,
            ILogger<DictionaryController> logger)
        {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Получение событий
        /// </summary>
        /// <param name="eventFilterGRPC"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async override Task<ListEvent> GetEvents(EventFilterGRPC eventFilterGRPC, ServerCallContext context)
        {
            _logger.LogInformation("Запрос событий");

            var eventFilterDTO = _mapper.Map<EventFilterGRPC, EventFilterDTO>(eventFilterGRPC);
            var res = await _service.GetEvents(eventFilterDTO);

            var result = new ListEvent()
            { 
                Status = "Success",
            };
            var dataGRPC = _mapper.Map<IEnumerable<Event>, IEnumerable<EventItemGRPC>>(res);
            result.Data.AddRange(dataGRPC);

            return result;
        }
    }
}
