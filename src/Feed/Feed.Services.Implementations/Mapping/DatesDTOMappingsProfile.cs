﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib;

namespace SKE.Feed.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности Даты.
    /// </summary>
    public class DatesDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public DatesDTOMappingsProfile()
        {
            CreateMap<DatePeriodGRPC, DatePeriodDTO>()
                .ForMember(x => x.Start, map => map.MapFrom(s => s.Start.ToDateTime()))
                .ForMember(x => x.End, map => map.MapFrom(s => s.End.ToDateTime()))
                ;
        }
    }
}
