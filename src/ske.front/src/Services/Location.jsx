import Constants from '../Constants';

export default async function getLocations()
{
    var url = `${Constants.GATEWAY_HOST}/EventManagerService/Dictionary/GetLocations`;
    var datas = await fetch(url,{
        method: 'GET',
      })
      .then(data => data.json());
      if(datas === undefined)
      {
        return {datas:[] };
      }
      return datas.data;
}