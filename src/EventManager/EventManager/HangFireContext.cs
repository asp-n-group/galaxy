﻿using Microsoft.EntityFrameworkCore;

namespace SKE.EventManager
{
    /// <summary>
    /// контекст планировщика задач
    /// </summary>
    public class HangFireContext
        : DbContext
    {
        /// <summary>
        /// ctor контекст планировщика задач
        /// </summary>
        public HangFireContext()
        {
        }

        /// <summary>
        /// контекст 
        /// </summary>
        public HangFireContext(DbContextOptions<HangFireContext> options)
            : base(options)
        {
        }
    }
}