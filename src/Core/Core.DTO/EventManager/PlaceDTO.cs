﻿namespace SKE.Core.DTO.EventManager
{
    public class PlaceDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// внешний идентификатор
        /// </summary>
        public int? ExternalId { get; set; }
        /// <summary>
        /// название
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// адрес
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// расписание
        /// </summary>
        public string TimeTable { get; set; }
        /// <summary>
        /// телефон
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// является ли заглушкой
        /// </summary>
        public bool IsStub { get; set; }
        /// <summary>
        /// полное описание
        /// </summary>
        public string BodyText { get; set; }
        /// <summary>
        /// описание
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// адрес места на сайте kudago.com
        /// </summary>
        public string SiteUrl { get; set; }
        /// <summary>
        /// сайт места
        /// </summary>
        public string ForeignUrl { get; set; }
        /// <summary>
        /// метро рядом
        /// </summary>
        public string SubWay { get; set; }
        /// <summary>
        /// число пользователей, добавивших место в избранное
        /// </summary>
        public int FavoritesCount { get; set; }
        /// <summary>
        /// число комментариев
        /// </summary>
        public int CommentsCount { get; set; }
        /// <summary>
        /// закрыто ли место
        /// </summary>
        public bool IsClosed { get; set; }
        /// <summary>
        /// короткое название
        /// </summary>
        public string ShortTitle { get; set; }

        /// <summary>
        /// город
        /// </summary>
        public CityDTO Location { get; set; }
        /// <summary>
        /// координаты места
        /// </summary>
        public CoordinatesDTO Coords { get; set; }
        /// <summary>
        /// галерея места
        /// </summary>
        public ICollection<ImageDTO> Images { get; set; }
        /// <summary>
        /// список категорий
        /// </summary>
        public ICollection<CategoryPlaceDTO> Categories { get; set; }
        /// <summary>
        /// тэги места
        /// </summary>
        public ICollection<TagDTO> Tags { get; set; }

        public PlaceDTO()
        {
        }
    }
}
