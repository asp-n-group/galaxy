﻿using SKE.Activity.Services.Contracts;
using AutoMapper;
using SKE.Activity.Domain;

namespace SKE.Activity.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности комментария.
    /// </summary>
    public class LikeDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor автомаппера для сущности комментария.
        /// </summary>
        public LikeDTOMappingsProfile()
        {
            CreateMap<Likes, LikeDTO>();
            CreateMap<LikeDTO, Likes>();
        }
    }
}
