﻿namespace SKE.Core.DTO.EventManager
{
    public class DatePeriodDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// начало
        /// </summary>
        public DateTime Start { get; set; }
        /// <summary>
        /// окончание
        /// </summary>
        public DateTime End { get; set; }
    }
}
