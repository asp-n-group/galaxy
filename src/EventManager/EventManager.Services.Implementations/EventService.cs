﻿using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Contracts;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Services.Implementations
{
    public class EventService : IEventService
    {
        IEventRepository _events;
        public EventService(IEventRepository events)
        {
            _events = events;
        }

        public async Task<IEnumerable<Event>> GetEvents(EventFilterDTO eventFilterDTO)
        {
            return await _events.GetFitleredAsync(eventFilterDTO);
        }
    }
}
