﻿using SKE.KudaGo.Domain.Model;
using SKE.KudaGo.Services.Repositories.Abstractions;

namespace SKE.KudaGo.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с категориями событий
    /// </summary>
    public class CategoryEventRepository : Repository<IEnumerable<CategoryEvent>>, ICategoryEventRepository
    {
        private readonly string _urlPath = "event-categories/?order_by=id&fields=id,slug,name";

        /// <summary>
        /// Получить список категорий событий
        /// </summary>
        /// <returns>список категорий событий</returns>
        public async Task<IEnumerable<CategoryEvent>> GetAllAsync()
        {
            return await GetAllQueryAsync(_urlPath);
        }
    }
}
