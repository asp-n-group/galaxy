﻿using SKE.Feed.Domain;
using SKE.Feed.Services.Contracts;

namespace SKE.Feed.Services.Abstractions
{
    public interface IFeedService
    {
        /// <summary>И
        /// Получить ленту
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во элементов на странице</param>
        /// <returns>DTO ленты</returns>
        Task<FeedDTO> GetByUserIdAsync(int userId, IEnumerable<string> userInterests, int page, int pageSize);

        /// <summary>И
        /// Получить ленту
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во элементов на странице</param>
        /// <returns>DTO ленты</returns>
        Task<FeedDTO> GetAsync(QueryParams queryParams);

        /// <summary>
        /// Создать ленту
        /// </summary>
        /// <param name="feedDTO">DTO ленты</param>
        /// <returns>Идентификатор новой ленты</returns>
        Task<int> Create(FeedDTO feedDTO);
    }
}
