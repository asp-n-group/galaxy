import * as React from 'react';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import { FixedSizeList, ListChildComponentProps } from 'react-window';
import {useState, useReducer} from 'react';

function renderRow(props: ListChildComponentProps) {
  const { index, style } = props;

  return (
    <ListItem style={style} key={index} component="div" disablePadding>
      <ListItemButton>
        <ListItemText primary={`Item ${index + 1}`} />
      </ListItemButton>
    </ListItem>
  );
}

export default function SkeList(props) {

  const [ignored, forceUpdate] = useReducer(x => x + 1, 0);
 
  let counter = 0;
  const intervalId = setInterval(() => {
    forceUpdate();
  }, 2000);
  return (
    <Box
      sx={{ width: '100%', height: '10%', maxWidth: 360, bgcolor: 'background.paper' }}
    >
      <List
        height={200}
        width={360}
        itemSize={46}
        itemCount={200}
        overscanCount={5}
      >
      {props.items().map((itemDatas, index) => (
             <ListItem  key={index} component="div" disablePadding>
             <ListItemButton>
               <ListItemText primary={`${itemDatas}`} />
             </ListItemButton>
           </ListItem>
      ))} 
      </List>
    </Box>
  );
}