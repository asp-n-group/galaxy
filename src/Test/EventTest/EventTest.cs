﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Contracts;
using SKE.EventManager.Services.Implementations;
using SKE.EventManager.Services.Repositories.Abstractions;
using System.Linq.Expressions;

namespace Test.EventTest
{
    public class EventTest
    {
        private readonly Mock<IEventRepository> _repoMock;
        private readonly IEventService _service;

        public EventTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _repoMock = fixture.Freeze<Mock<IEventRepository>>();
            _service = fixture.Build<EventService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetEventAsyncTests_CheckCategories_ReturnEnumerable()
        {
            // Arrange
            var data = CreateData();
            var param = new EventFilterDTO() { IncludeCategories = true };
            _repoMock.Setup(repo => repo.GetFitleredAsync(param)).ReturnsAsync(data);

            // Act
            var result = await _service.GetEvents(param);
            var element = result.FirstOrDefault(x => x.Id == Guid.Parse("20cc2599-a36d-41af-8b78-b14cd6563252"));

            // Assert
            result.Should().BeAssignableTo<List<Event>>();
            element.Should().NotBeNull();
            element.Categories.Should().NotBeNull();
            element.Categories.Count().Should().NotBe(0);
        }

        [Fact]
        public async void GetEventAsyncTests_CheckDates_ReturnEnumerable()
        {
            // Arrange
            var data = CreateData();
            var param = new EventFilterDTO() { IncludeDates = true };
            _repoMock.Setup(repo => repo.GetFitleredAsync(param)).ReturnsAsync(data);

            // Act
            var result = await _service.GetEvents(param);
            var element = result.FirstOrDefault(x => x.Id == Guid.Parse("20cc2599-a36d-41af-8b78-b14cd6563252"));

            // Assert
            result.Should().BeAssignableTo<List<Event>>();
            element.Should().NotBeNull();
            element.Dates.Should().NotBeNull();
            element.Dates.Count().Should().NotBe(0);
        }

        [Fact]
        public async void GetEventAsyncTests_CheckImages_ReturnEnumerable()
        {
            // Arrange
            var data = CreateData();
            var param = new EventFilterDTO() { IncludeImages = true };
            _repoMock.Setup(repo => repo.GetFitleredAsync(param)).ReturnsAsync(data);

            // Act
            var result = await _service.GetEvents(param);
            var element = result.FirstOrDefault(x => x.Id == Guid.Parse("20cc2599-a36d-41af-8b78-b14cd6563252"));

            // Assert
            result.Should().BeAssignableTo<List<Event>>();
            element.Should().NotBeNull();
            element.Images.Should().NotBeNull();
            element.Images.Count().Should().NotBe(0);
        }

        [Fact]
        public async void GetEventAsyncTests_CheckLocation_ReturnObject()
        {
            // Arrange
            var data = CreateData();
            var param = new EventFilterDTO() { IncludeLocation = true };
            _repoMock.Setup(repo => repo.GetFitleredAsync(param)).ReturnsAsync(data);

            // Act
            var result = await _service.GetEvents(param);
            var element = result.FirstOrDefault(x => x.Id == Guid.Parse("20cc2599-a36d-41af-8b78-b14cd6563252"));

            // Assert
            result.Should().BeAssignableTo<List<Event>>();
            element.Should().NotBeNull();
            element.Location.Should().NotBeNull();
        }

        [Fact]
        public async void GetEventAsyncTests_CheckPlace_ReturnObject()
        {
            // Arrange
            var data = CreateData();
            var param = new EventFilterDTO() { IncludePlace = true };
            _repoMock.Setup(repo => repo.GetFitleredAsync(param)).ReturnsAsync(data);

            // Act
            var result = await _service.GetEvents(param);
            var element = result.FirstOrDefault(x => x.Id == Guid.Parse("20cc2599-a36d-41af-8b78-b14cd6563252"));

            // Assert
            result.Should().BeAssignableTo<List<Event>>();
            element.Should().NotBeNull();
            element.Place.Should().NotBeNull();
        }


        [Fact]
        public async void GetEventAsyncTests_CheckTags_ReturnEnumerable()
        {
            // Arrange
            var data = CreateData();
            var param = new EventFilterDTO() { IncludeTags = true };
            _repoMock.Setup(repo => repo.GetFitleredAsync(param)).ReturnsAsync(data);

            // Act
            var result = await _service.GetEvents(param);
            var element = result.FirstOrDefault(x => x.Id == Guid.Parse("20cc2599-a36d-41af-8b78-b14cd6563252"));

            // Assert
            result.Should().BeAssignableTo<List<Event>>();
            element.Should().NotBeNull();
            element.Tags.Should().NotBeNull();
            element.Tags.Count().Should().NotBe(0);
        }

        private List<Event> CreateData()
        {
            return new List<Event> {
                new Event {
                    AgeRestriction = "0",
                    BodyText = "Выставки",
                    Categories = new List<CategoryEvent>{
                        new  CategoryEvent {
                            Id = Guid.Parse("14e0dad5-49bc-45fe-9a07-3b338a729261"),
                            ExternalId = 4,
                            Slug = "party",
                            Name = "Вечеринки"
                        },
                        new  CategoryEvent {
                            Id = Guid.Parse("3037b899-0d82-49a4-a393-fe5211bfc16d"),
                            ExternalId = 7,
                            Slug = "tour",
                            Name = "Экскурсии"
                        },
                    },
                    Dates = new List<DatePeriod> {
                        new DatePeriod {
                            Id = Guid.Parse("143562d0-307a-4eff-9831-8790feae5731"),
                            Start = new DateTime(2021,5,22,15,0,0),
                            End = new DateTime(2021,5,24,3,0,0)
                        },
                        new DatePeriod  {
                            Id = Guid.Parse("258681de-cb64-4a2e-a06d-f89c9059266c"),
                            Start = new DateTime(2022,5,21,15,0,0),
                            End = new DateTime(2022,5,22,3,0,0)
                        },
                    },
                    CommentsCount = 3,
                    Description = "Только один раз",
                    ExternalId = 178324,
                    FavoritesCount = 184,
                    Id = Guid.Parse("20cc2599-a36d-41af-8b78-b14cd6563252"),
                    Images = new List<Image> {
                        new Image {
                            Id = Guid.Parse("687d43fa-c99f-414b-9567-bb5fd37786c7"),
                            ImageUrl = "https://kudago.com/media/images/event/f5/74/f57432046e5e293911fc2826f77d7065.jpeg",
                            Thumbnails = new Thumbnail {
                                Id = Guid.Parse("32631b7f-cc9a-4d50-8e97-505b0271aaac"),
                                Url640x384 = "https://kudago.com/media/thumbs/640x384/images/event/f5/74/f57432046e5e293911fc2826f77d7065.jpeg",
                                Url144x96 = "https://kudago.com/media/thumbs/144x96/images/event/f5/74/f57432046e5e293911fc2826f77d7065.jpeg"
                            },
                        },
                        new Image    {
                            Id = Guid.Parse("82d2ba3c-9adc-43a9-8021-d592c86afd98"),
                            ImageUrl = "https://kudago.com/media/images/event/db/4a/db4a6413ea4e71aba2551f2ae3c6c834.jpg",
                            Thumbnails = new Thumbnail {
                                Id = Guid.Parse("fd84bdb3-b140-4f03-8fb1-38fec3a2b5c7"),
                                Url640x384 = "https://kudago.com/media/thumbs/640x384/images/event/db/4a/db4a6413ea4e71aba2551f2ae3c6c834.jpg",
                                Url144x96 = "https://kudago.com/media/thumbs/144x96/images/event/db/4a/db4a6413ea4e71aba2551f2ae3c6c834.jpg"
                            },
                        }
                    },
                    IsFree = false,
                    Location = new City {
                        Id = Guid.Parse("5878ae43-80bf-4890-9b3d-7c22f723ed19"),
                        Slug = "spb",
                        Name = "Санкт-Петербург",
                        Timezone ="GMT+03:00",
                        Language = "ru",
                        Currency = "RUB"
                    },
                    Place = new Place {
                        Id = Guid.Parse("196e7b04-ba19-47d2-8aaa-4f9dda76ef4d"),
                        ExternalId = 198,
                        Title = "Дворцовая площадь",
                        Slug = "dvorcovaya-ploshad",
                        Address = "Дворцовая пл.",
                        TimeTable = "",
                        Phone = "",
                        IsStub = false,
                        BodyText = "Если спросить",
                        Description = "Самая красивая площадь России и одна из красивейших в мире.",
                        SiteUrl = "https://kudago.com/spb/place/dvorcovaya-ploshad/",
                        ForeignUrl = "",
                        SubWay = "Адмиралтейская",
                        FavoritesCount = 220,
                        CommentsCount = 2,
                        IsClosed = false,
                        ShortTitle = "Дворцовая площадь",
                    },
                    Price = "от 250 до 550 рублей",
                    PublicationDate = new DateTime(2019,03,28,10,10,03),
                    ShortTitle = "Ночь музеев — 2023",
                    Slug = "vyistavka-noch-muzeev-2019",
                    TagLine = "",
                    Tags = new List<Tag>{
                        new Tag {
                            Id = Guid.Parse("09ca5d9b-4aa6-4d98-aa20-05af297ce7a1"),
                            Name = "самые интересные события в городе"
                        },
                        new Tag {
                            Id = Guid.Parse("11c89caa-74f4-43ea-9a11-2f17301e7f00"),
                            Name = "выставки"
                        },
                    },
                    Title = "Ночь музеев — 2023"
                }
            };
        }
    }
}