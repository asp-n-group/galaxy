﻿using Microsoft.EntityFrameworkCore;
using SKE.Core.Repositories.Implementations;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Repositories.Implementations
{
    public class GenreRepository : Repository<Genre, Guid>, IGenreRepository
    {
        public GenreRepository(DataContext context) : base(context)
        {
        }

        public Task<List<Genre>> GetFitleredAsync()
        {
            return GetAll().ToListAsync();
        }
    }
}