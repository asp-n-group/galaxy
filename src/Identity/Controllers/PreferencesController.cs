﻿using Identity.Data;
using Identity.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Identity.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly AuthDbContext _dbContext;

        public PreferencesController(AuthDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Получить все интересы
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Preference>>> GetAllPreferences()
        {
            var preferences = await _dbContext.Preference.ToListAsync();
            return Ok(preferences);
        }

        /// <summary>
        /// Добавить новый интерес
        /// </summary>
        /// <param name="preference"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddPreference(Preference preference)
        {
            bool exists = await _dbContext.Preference.AnyAsync(p => p.Name == preference.Name);
            if (exists)
            {
                return BadRequest(new { message = "Интерес уже существует." });
            }

            _dbContext.Preference.Add(preference);
            await _dbContext.SaveChangesAsync();

            return Ok(new { message = "Интерес добавлен.", id = preference.Id });
        }

        /// <summary>
        /// Удалить интерес
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePreference(int id)
        {
            var preference = await _dbContext.Preference.FindAsync(id);
            if (preference == null)
            {
                return NotFound();
            }

            _dbContext.Preference.Remove(preference);
            await _dbContext.SaveChangesAsync();

            return Ok(new { message = "Интерес удален." });
        }
    }
}