﻿namespace SKE.Core.DTO.EventManager
{
    public class MovieShowingsDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// внешний идентификатор
        /// </summary>
        public int? ExternalId { get; set; }
        /// <summary>
        /// время показа
        /// </summary>
        public DateTime ShowingsDate { get; set; }
        /// <summary>
        /// признак 3D
        /// </summary>
        public bool Is3D { get; set; }
        /// <summary>
        /// признак IMAX
        /// </summary>
        public bool IsImax { get; set; }
        /// <summary>
        /// признак 4D
        /// </summary>
        public bool Is4D { get; set; }
        /// <summary>
        /// язык оригинала
        /// </summary>
        public bool OriginalLanguage { get; set; }
        /// <summary>
        /// стоимость
        /// </summary>
        public string Price { get; set; }
    }
}
