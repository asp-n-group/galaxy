﻿using Hangfire;
using Hangfire.Dashboard;
using Hangfire.PostgreSql;
using SKE.EventManager.Services.Abstractions;

namespace SKE.EventManager
{
    /// <summary>
    /// расширение регистрации
    /// </summary>
    public static class RegistrationBuilder
    {
        /// <summary>
        /// настройка hangfire
        /// </summary>
        public static IHost InstallHangfire(this IHost host, IConfiguration configuration)
        {
            JobStorage.Current = new PostgreSqlStorage(configuration.GetConnectionString("HangFireConnection"));
            using (var serviceScope = host.Services.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                var manager = new RecurringJobManager();
                // запуск синхронизации данных
                manager.AddOrUpdate(
                    "Full Sync",
                    () => services.GetService<ISyncService>().SyncData(),
                    "0 0 1 * * ?",
                    TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"),
                    "eventmanager"
                    );
            }
            return host;
        }

        /// <summary>
        /// панель управления
        /// </summary>
        public static IApplicationBuilder InstallHangfireDashboard(this IApplicationBuilder host)
        {
            host.UseHangfireDashboard(
                "/hangfire", new DashboardOptions
                {
                    Authorization = new[] { new MyAuthorizationFilter() { } }
                    // IsReadOnlyFunc = (DashboardContext context) => true
                });
            return host;
        }
    }

    /// <summary>
    /// класс авториизациии
    /// </summary>
    public class MyAuthorizationFilter : IDashboardAuthorizationFilter
    {
        /// <summary>
        /// авторизация
        /// </summary>
        public bool Authorize(DashboardContext context)
        {
            return true;
        }
    }
}
