﻿namespace SKE.Circle.Services.Contracts
{
    public class CircleInfoDTO
    {
        public Guid Id { get; set; }

        /// <summary>
        /// имя круга
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// список пользователей в круге
        /// </summary>
        public ICollection<string> Users { get; set; } = new List<string>();

        /// <summary>
        /// список событиий в круге
        /// </summary>
        public ICollection<EventInfoDTO> Events { get; set; } = new List<EventInfoDTO>();
    }
}