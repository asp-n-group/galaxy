﻿using SKE.CoreLib.Core;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Координаты
    /// </summary>
    public class Coordinate : BaseEntity
    {
        /// <summary>
        /// широта
        /// </summary>
        [Column(TypeName = "decimal(19, 15)")]
        public decimal? Lat { get; set; }
        /// <summary>
        /// высота
        /// </summary>
        [Column(TypeName = "decimal(19, 15)")]
        public decimal? Lon { get; set; }

        /// <summary>
        /// идентификатор города
        /// </summary>
        public Guid? CityId { get; set; }
        /// <summary>
        /// города
        /// </summary>
        public City? City { get; set; }

        /// <summary>
        /// идентификатор места
        /// </summary>
        public Guid? PlaceId { get; set; }
        /// <summary>
        /// место
        /// </summary>
        public Place? Place { get; set; }
    }
}
