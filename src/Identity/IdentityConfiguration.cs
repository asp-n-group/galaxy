﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Security.Claims;

namespace Identity
{
    public class IdentityConfiguration
    {
        public static List<TestUser> TestUsers =>
            new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "123",
                    Username = "Ivan",
                    Password = "Ivan@ivan.iv",
                    Claims =
                    {
                        new Claim(JwtClaimTypes.Name, "Ivan"),
                        new Claim(JwtClaimTypes.GivenName, "Ivan"),
                        new Claim(JwtClaimTypes.FamilyName, "Ivan")
                    }
                }
              };

        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("api.read"),
                new ApiScope("api.write"),
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new ApiResource("myApi")
                {
                    Scopes = new List<string>{ "Api.read","Api.write" },
                    ApiSecrets = new List<Secret>{ new Secret("secret".Sha256()) }
                }
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "client",
                    ClientName = "Client Credentials Client",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedScopes = { "api.read" },
                    RedirectUris =
                    {
                        "https://localhost:60009/signin-oidc"
                    },
                    AllowedCorsOrigins =
                    {
                        "https://localhost:60009"
                    },
                    PostLogoutRedirectUris =
                    {
                        "https://localhost:60009/signout-oidc"
                    },
                    AllowAccessTokensViaBrowser = true
                },
            };


    }
}
