﻿using AutoMapper;
using EF = SKE.EventManager.Domain;
using KG = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности координат.
    /// </summary>
    public class CoordinateMappings : Profile
    {
        public CoordinateMappings()
        {
            CreateMap<KG.Coordinates, EF.Coordinate>();
        }
    }
}
