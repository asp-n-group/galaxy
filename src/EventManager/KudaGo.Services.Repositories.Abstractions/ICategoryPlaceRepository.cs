﻿using SKE.KudaGo.Domain.Model;

namespace SKE.KudaGo.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с категориями мест
    /// </summary>
    public interface ICategoryPlaceRepository : IRepository<IEnumerable<CategoryPlace>>
    {
        /// <summary>
        /// Запросить все сущности 
        /// </summary>
        /// <returns> массив сущностей</returns>
        Task<IEnumerable<CategoryPlace>> GetAllAsync();
    }
}