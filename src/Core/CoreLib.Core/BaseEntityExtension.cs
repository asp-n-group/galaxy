﻿namespace SKE.CoreLib.Core
{
    /// <summary>
    /// Расширенная часть для моделей БД, с внешним ключем
    /// </summary>
    public class BaseEntityExtension : BaseEntity
    {
        /// <summary>
        /// внешний идентификатор
        /// </summary>
        public int? ExternalId { get; set; }
    }
}
