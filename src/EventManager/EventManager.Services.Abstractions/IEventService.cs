﻿using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Services.Abstractions
{
    public interface IEventService
    {
        Task<IEnumerable<Event>> GetEvents(EventFilterDTO eventFilterDTO);
    }
}
