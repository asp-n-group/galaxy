﻿
using SKE.Core.Repositories.Abstractions;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с фильмами
    /// </summary>
    public interface IMovieRepository : IRepository<Movie, Guid>
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <param name="filterDTO"> ДТО фильтра. </param>
        /// <returns> Список фильмов. </returns>
        Task<List<Movie>> GetFitleredAsync(MovieFilterDTO filterDTO);
    }
}
