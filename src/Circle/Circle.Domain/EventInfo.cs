﻿using SKE.CoreLib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKE.Circle.Domain
{
    /// <summary>
    /// Модель информации о событии в круге
    /// </summary>
    public class EventInfo : BaseEntity
    {
        /// <summary>
        /// Дата добавления в круг
        /// </summary>
        public DateTime DateAdd { get; set; }

        /// <summary>
        /// ссылка на событие
        /// </summary>
        public Guid EventId { get; set; }
    }
}
