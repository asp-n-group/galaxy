using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SKE.EmailNotificator.Domain;
using SKE.EmailNotifiicator.Services.Repositories.Abstractions;

namespace SKE.EmailNotificator.Host.Controllers
{
    /// <summary>
    /// ��������� �������� ���������
    /// </summary>
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class SendEmailController : ControllerBase
    {
        private IEmailSendingService _emailSendingService;
        public SendEmailController(IEmailSendingService emailSendingService)
        {
            _emailSendingService = emailSendingService;
        }

        /// <summary>
        /// ��������� ���������
        /// </summary>
        [HttpPost("SendEmail")]
        public async Task<IActionResult> SendEmail(SendEmailRequest request)
        {
            await _emailSendingService.SendEmailAsync(request);
            return Ok();
        }
    }
}