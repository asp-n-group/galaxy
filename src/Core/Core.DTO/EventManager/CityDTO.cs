﻿namespace SKE.Core.DTO.EventManager
{
    public class CityDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// название
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// часовой пояс
        /// </summary>
        public string Timezone { get; set; }
        /// <summary>
        /// координаты
        /// </summary>
        public CoordinatesDTO Coords { get; set; }
        /// <summary>
        /// язык
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// валюта
        /// </summary>
        public string Currency { get; set; }
    }
}
