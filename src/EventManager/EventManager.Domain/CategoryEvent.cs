﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Категории событий
    /// </summary>
    public class CategoryEvent : BaseEntityExtension
    {
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// связь категорий с событиями
        /// </summary>
        public ICollection<CategoryToEvent> CategoryToEvents { get; set; }
        /// <summary>
        /// события
        /// </summary>
        public ICollection<Event> Events { get; set; }
    }
}
