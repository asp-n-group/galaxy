
namespace SKE.Circle.Domain
{
    public class CircleModel
    {
        /// <summary>
        /// имя круга
        /// </summary>  
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// список пользователей в круге
        /// </summary>
        public string User {get;set;}= string.Empty;
    }
}