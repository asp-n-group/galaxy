﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;

namespace Identity.Models
{
    public class Preference : INotifyPropertyChanging, INotifyPropertyChanged
    {
        private int id;
        private string? name;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonIgnore]
        public virtual int Id
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    OnPropertyChanging();
                    id = value;
                    OnPropertyChanged();
                }
            }
        }

        public virtual string? Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    OnPropertyChanging();
                    name = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangingEventHandler? PropertyChanging;
        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void OnPropertyChanging([CallerMemberName] string? propertyName = null)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
