﻿using SKE.Core.Repositories.Abstractions;
using SKE.EventManager.Domain;

namespace SKE.EventManager.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы городами
    /// </summary>
    public interface ICityRepository : IRepository<City, Guid>
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <returns> Список городов. </returns>
        Task<List<City>> GetFitleredAsync();
    }
}
