﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class ImageDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public ImageDTOMappingsProfile()
        {
            CreateMap<ImageDTO, EF.Image>();
            CreateMap<EF.Image, ImageDTO>();

            CreateMap<EF.Image, ImageGRPC>();
        }
    }
}
