import * as React from 'react';
import { useState, useEffect } from 'react';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import Badge from '@mui/material/Badge';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MailIcon from '@mui/icons-material/Mail';
import NotificationsIcon from '@mui/icons-material/Notifications';
import MoreIcon from '@mui/icons-material/MoreVert';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { Link } from '@mui/material';
import Icon from '@mui/material/Icon';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import LoginIcon from '@mui/icons-material/Login';
import useToken from '../UseToken';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import UserService from '../Services/UserService';
import CircleService from '../Services/CircleService';
import Locations from './Location';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

export default function SkeAppBar(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const [autentificated, SetAuth] = React.useState(useToken().token != undefined);

  const [age, setCircle] = React.useState('');
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const [circles, setCircles] = React.useState([{users:[]}]);
  const [loaded, setLoaded] = React.useState(false);

  useEffect(() => {
    if(loaded)
    {
      return;
    }
    setLoaded(true);
    const datap = async ()=>{
      var d = await CircleService.getUserCircles();
      setCircles(d);
    };
    datap();
  },[circles, loaded]);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMenuExit = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
    UserService.clearSession();
    window.location.href = '/';
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleLogin = (event) =>{
    window.location.href = '/login';
  }

  const handleRegistration = (event) =>{
    window.location.href = '/registration';
  }
  const GroupAddButton = () => 
  {
    if(UserService.IsAuthorized)
    {
      return ( <GroupAddIcon cursor="pointer" fontSize='large' sx={{my: 2, mx:2, color: 'white', display: 'block' }}   size="small"  width='100'/>);
    }
  };

  const handleProfile = () =>
  {
    window.location.href = '/profile';
  };

  const CircleSelection = ()=>{
    const handleChange = (event) => {
      setCircle(event.target.value);
    };
    
    if(UserService.IsAuthorized() === false)
    {
      return (<Box/>);
    }
    
    return ( 
      <FormControl sx={{my: 1, mx:2, color: 'white', display: 'block', minWidth: 120 }}   size="small"  >
        <InputLabel sx={{ color: 'white', display: 'block' }}  id="demo-simple-select-label">Круг</InputLabel>
        <Select sx={{ my: 1, color: 'white', display: 'block' }} labelId="demo-simple-select-label"
          id="demo-simple-select" label="Круг" onChange={handleChange}>
          {circles.map((u, index) => (
            <MenuItem key={index} value={u.name} >{u.name}</MenuItem> 
          ))} 
        </Select>
      </FormControl> 
    );
  };

  var pages = undefined;

  if(UserService.IsAuthorized())
  {
    pages = [
      {id:'registred',name:'Лента',},
      {id:'help',name:'Помощь',}, 
      {id:'about',name:'О Нас',}, 
    ];
  }
  else
  {
    pages = [
      {id:'registred',name:'Лента',},
      {id:'help',name:'Помощь',}, 
      {id:'about',name:'О Нас',}, 
    ];
  }
  const OnPageClick = (event) =>
  {
    window.location.href = '/'+event.target.id;
  }
  
  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu anchorEl={anchorEl} anchorOrigin={{ vertical: 'top', horizontal: 'right', }}
        id={menuId} keepMounted transformOrigin={{ vertical: 'top', horizontal: 'right',}}
        open={isMenuOpen} onClose={handleMenuClose} sx={{ zIndex:132234}}>
      <MenuItem onClick={handleProfile}>Профиль</MenuItem>
      <MenuItem onClick={handleMenuClose}>Настройки</MenuItem>
      <MenuItem onClick={handleMenuExit}>Выход</MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu anchorEl={mobileMoreAnchorEl} anchorOrigin={{ vertical: 'top', horizontal: 'right', }}
        id={mobileMenuId} keepMounted transformOrigin={{ vertical: 'top', horizontal: 'right', }}
        open={isMobileMenuOpen} onClose={handleMobileMenuClose}>
      { pages.map((page) => (
          <MenuItem id={page.id} key={page.id} onClick={handleCloseNavMenu}>
            <IconButton size="large" aria-label="account of current user" aria-controls="primary-search-account-menu"
              aria-haspopup="true" color="inherit">
              <AccountCircle />
            </IconButton>
            <Typography textAlign="center">{page.name}</Typography>
          </MenuItem>
      ))}
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton size="large" aria-label="account of current user" aria-controls="primary-search-account-menu"
            aria-haspopup="true" color="inherit">
          <AccountCircle />
        </IconButton>
        <p>Профиль</p>
      </MenuItem>
    </Menu>
  );

  const ShowProfile = (auth) => {
    if (auth === true) {
      return (
        <IconButton size="large" edge="end" aria-label="account of current user" aria-controls={menuId}
            aria-haspopup="true" onClick={handleProfileMenuOpen} color="inherit">
          <AccountCircle />
        </IconButton>);
    }
  }

  const ShowLogin = (auth) => {
    if (auth === false) {
      return (
        <Box>
          <IconButton size="large" edge="end" aria-label="Логин" aria-controls={menuId} aria-haspopup="true" 
              onClick={handleLogin} color="inherit" >
            <LoginIcon />
          </IconButton>
          <IconButton size="large" edge="end" aria-label="Регистрация" aria-controls={menuId} aria-haspopup="true"
              onClick={handleRegistration} color="inherit" >
            <AppRegistrationIcon />
          </IconButton>
        </Box>
      );
    }
  }

  const handleRoot = ()=>{
    if(UserService.IsAuthorized())
    {
      window.location.href = '/registred';
    }
    else
    {
      window.location.href = '/';
    }
  };

  return (
    <Box sx={{ flexGrow: 1 }} >
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      <AppBar position="static">
        <Toolbar>
          <Box sx={{ display: { xs: 'none', md: 'flex', width:'20%'} }}>
            <Locations selectedLocation={props.selectedLocation} selectLocation={props.selectLocation}/>
          </Box>
          <Box display="flex" justifyContent="center" sx={{ display: { xs: 'none', md: 'flex', width:'40%'} }}>
            <Link onClick={handleRoot}  sx={{cursor:'pointer'}}>
              <Typography variant="h6" noWrap component="div" sx={{ display: { xs: 'none', sm: 'block' , color:'white' } }}>
                ASP-N-TEAM
              </Typography>
            </Link>
          </Box>
          <Box sx={{ display: { xs: 'none', md: 'flex', width:'40%'} }}>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
              {pages.map((page) => (
                <Button id={page.id} key={page.id} onClick={OnPageClick} sx={{ my: 1, mx:2,color: 'white', display: 'block' }}>
                  {page.name}
                </Button>
              ))}
            </Box>
            <Box sx={{ my: 1, mx:2,color: 'white', display: 'block' }}>
              {ShowProfile(autentificated)}
              {ShowLogin(autentificated)}
            </Box>
          </Box>
          <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
            <IconButton size="large" aria-label="show more" aria-controls={mobileMenuId} aria-haspopup="true" 
                onClick={handleMobileMenuOpen} color="inherit">
              <MoreIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </Box>
  );
}