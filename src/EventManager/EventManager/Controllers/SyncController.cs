﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SKE.EventManager.Services.Abstractions;

namespace SKE.EventManager.Controllers
{
    /// <summary>
    /// контроллер синхронизации данных
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SyncController : ControllerBase
    {

        ISyncService _syncService;
        private readonly ILogger<SyncController> _logger;

        /// <summary>
        /// ctor контроллер синхронизации данных
        /// </summary>
        public SyncController(ISyncService syncService, ILogger<SyncController> logger)
        {
            _syncService = syncService;
            _logger = logger;
        }
        /// <summary>
        /// Получить данные из кудаго
        /// </summary>
        /// <returns></returns>
        [HttpGet("Get")]
        public async Task Get()
        {
            _logger.LogInformation("Синхронизация данных KudaGo");
            await _syncService.SyncData();
        }
    }
}
