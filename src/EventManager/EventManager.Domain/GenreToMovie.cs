﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Связь многие ко многим жанры и фильмы
    /// </summary>
    public class GenreToMovie : BaseEntity
    {
        /// <summary>
        /// идентификатор жанра 
        /// </summary>
        public Guid GenreId { get; set; }
        /// <summary>
        /// жанр
        /// </summary>
        public Genre Genre { get; set; }

        /// <summary>
        /// идентификатор фильма
        /// </summary>
        public Guid MovieId { get; set; }
        /// <summary>
        /// фильм
        /// </summary>
        public Movie Movie { get; set; }
    }
}
