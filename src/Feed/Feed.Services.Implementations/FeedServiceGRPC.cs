﻿using AutoMapper;
using Google.Protobuf.Collections;
using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib;
using SKE.Feed.Domain;
using SKE.Feed.Services.Abstractions;
using SKE.Feed.Services.Contracts;

namespace SKE.Feed.Services.Implementations
{
    /// <summary>
    /// Cервис работы с лентами
    /// </summary>
    public class FeedServiceGRPC : IFeedService
    {
        private readonly IMapper _mapper;
        private readonly IUserInterestsService _userInterestsService;
        private readonly IConfiguration _configuration;

        public FeedServiceGRPC(IMapper mapper, IUserInterestsService userInterestsService, IConfiguration configuration)
        {
            _mapper = mapper;
            _userInterestsService = userInterestsService;
            _configuration = configuration;
        }

        /// <summary>
        /// Получить ленту
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во элементов на странице</param>
        /// <returns>DTO ленты</returns>
        public async Task<FeedDTO> GetByUserIdAsync(int userId, IEnumerable<string> userInterests, int page, int pageSize)
        {
            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;

            var eventManagerApiParam = IsDebug ? "EventManagerApiDebug" : "EventManagerApi";
            using var channel = GrpcChannel.ForAddress(_configuration[eventManagerApiParam],
                new GrpcChannelOptions { HttpHandler = handler });

            var eventClient = new EventGRPC.EventGRPCClient(channel);
            var eventFilter = new EventFilterGRPC()
            {
                IncludeDates = true,
                IncludeImages = true,
                IncludeLocation = true,
                IncludePlace = true,
                Page = page,
                PageSize = pageSize,
            };

            eventFilter.Categories.AddRange(userInterests.Select(x=>_userInterestsService.CorrectInterest(x)));

            var eventsGRPC = await eventClient.GetEventsAsync(eventFilter);

            var feed = new Domain.Feed()
            {
                FeedEvents = _mapper.Map<IEnumerable<EventItemGRPC>, IEnumerable<FeedEvent>>(eventsGRPC.Data).ToList()

            };

            return _mapper.Map<Domain.Feed, FeedDTO>(feed);
        }

        /// <summary>
        /// Создать ленту
        /// </summary>
        /// <param name="feedDTO">DTO ленты</param>
        /// <returns>Идентификатор нового ленты</returns>
        public Task<int> Create(FeedDTO feedDTO)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получить ленту
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во элементов на странице</param>
        /// <returns>DTO ленты</returns>
        public async Task<FeedDTO> GetAsync(QueryParams queryParams)
        {
            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;

            var eventManagerApiParam = IsDebug ? "EventManagerApiDebug" : "EventManagerApi";
            using var channel = GrpcChannel.ForAddress(_configuration[eventManagerApiParam],
                new GrpcChannelOptions { HttpHandler = handler });

            var eventClient = new EventGRPC.EventGRPCClient(channel);
            var eventFilter = new EventFilterGRPC()
            {
                IncludeDates = true,
                IncludeImages = true,
                IncludeLocation = true,
                IncludePlace = true,
                Page = queryParams.page,
                PageSize = queryParams.pageSize,
            };

            if(queryParams.eventsId != null && queryParams.eventsId.Count() != 0)
            {
                eventFilter.EventsId.AddRange(queryParams.eventsId.Select(x=>x.ToString()).ToList());
            }
            if (queryParams.categories != null && queryParams.categories.Count() != 0)
            {
                eventFilter.Categories.AddRange(queryParams.categories.Select(x => x.ToString()).ToList());
            }
            if (!String.IsNullOrEmpty(queryParams.city))
            {
                eventFilter.CityId = queryParams.city;
            }

            var eventsGRPC = await eventClient.GetEventsAsync(eventFilter);

            var feed = new Domain.Feed()
            {
                FeedEvents = _mapper.Map<IEnumerable<EventItemGRPC>, IEnumerable<FeedEvent>>(eventsGRPC.Data).ToList()
            };

            return _mapper.Map<Domain.Feed, FeedDTO>(feed);
        }

        public static bool IsDebug
        {
            get
            {
                bool isDebug = false;
                #if DEBUG
                       isDebug = true;
                #endif
                return isDebug;
            }
        }
    }
}