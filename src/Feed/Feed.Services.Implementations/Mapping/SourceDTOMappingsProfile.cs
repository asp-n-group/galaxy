﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;

namespace SKE.Feed.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class SourceDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public SourceDTOMappingsProfile()
        {
            CreateMap<SourceGRPC, SourceDTO>();
        }
    }
}
