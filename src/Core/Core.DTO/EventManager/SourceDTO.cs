﻿namespace SKE.Core.DTO.EventManager
{
    public class SourceDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// имя источника
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// ссылка
        /// </summary>
        public string Link { get; set; }
    }
}
