﻿using SKE.KudaGo.Domain.Model;
using SKE.KudaGo.Services.Repositories.Abstractions;

namespace SKE.KudaGo.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с расписанием фильмов
    /// </summary>
    public class MovieShowingsRepository : Repository<KudaGoResponse<MovieShowings>>, IMovieShowingsRepository
    {
        bool hasNextPage;
        int pageNumber = 1;

        private readonly string _urlPathPrefix = "movies/";
        private readonly string _urlPathPostfix = "/showings/?" +
            "&page_size=100" +
            "&fields=id,movie,place,datetime,three_d,imax,four_dx,original_language,price" +
            "&expand=movie,place" +
            "&order_by=id";

        /// <summary>
        /// Получить список расписаний фильмов
        /// </summary>
        /// <returns>список расписаний фильмов</returns>
        public async IAsyncEnumerable<IEnumerable<MovieShowings>> GetAllAsync(int movieId)
        {
            do
            {
                var requestString = _urlPathPrefix + movieId.ToString() + _urlPathPostfix + "&page=" + pageNumber;
                var response = await GetAllQueryAsync(requestString);
                yield return response.Results;
                hasNextPage = response.Next is null ? false : true;
                pageNumber++;
            }
            while (hasNextPage);
        }
    }
}
