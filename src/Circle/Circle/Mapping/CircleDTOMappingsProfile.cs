﻿using SKE.Circle.Services.Contracts;
using AutoMapper;
using SKE.Circle.Domain;

namespace SKE.Circle.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности круга.
    /// </summary>
    public class CircleDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor Профиль автомаппера для сущности круга
        /// </summary>
        public CircleDTOMappingsProfile()
        {
            CreateMap<CircleInfo, CircleInfoDTO>();
            CreateMap<CircleInfoDTO, CircleInfo>();
        }
    }
}
