﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib;
using SKE.Core.gRPCLib.Shared;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class CityDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public CityDTOMappingsProfile()
        {
            CreateMap<CityDTO, EF.City>();
            CreateMap<EF.City, CityDTO>();

            CreateMap<EF.City, LocationGRPC>()
                .ForMember(x => x.Id, map => map.MapFrom(s => s.Id.ToString()));
        }
    }
}
