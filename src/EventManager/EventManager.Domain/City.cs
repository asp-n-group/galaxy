﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Список городов
    /// </summary>
    public class City : BaseEntity
    {
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// название
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// часовой пояс
        /// </summary>
        public string Timezone { get; set; }
        /// <summary>
        /// координаты
        /// </summary>
        public Coordinate Coords { get; set; }
        /// <summary>
        /// язык
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// валюта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// места
        /// </summary>
        public ICollection<Place> Places { get; set; }

        /// <summary>
        /// события
        /// </summary>
        public ICollection<Event> Events { get; set; }
    }
}
