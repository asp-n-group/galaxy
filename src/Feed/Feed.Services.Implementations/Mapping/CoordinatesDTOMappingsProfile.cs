﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;

namespace SKE.Feed.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class CoordinatesDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public CoordinatesDTOMappingsProfile()
        {
            CreateMap<CoordinatesGRPC, CoordinatesDTO>();
        }
    }
}
