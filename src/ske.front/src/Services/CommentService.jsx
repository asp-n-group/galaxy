import Constants from '../Constants';
import UseToken from '../UseToken';

export default {
  async getCommentsByEventCircle(circeId, eventId)
  {
    var token = UseToken().token;
    var url = `${Constants.GATEWAY_HOST}/ActivityService/Comments/GetComments?circeId=${circeId}&eventId=${eventId}`;
    var datas = await fetch(url,{
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    })
    .then(data => data.json());
    if(datas === undefined)
    {
      return [];
    }
    return datas;
  },
  async addComment(eventId, circeId, userId, message, userName)
  {
    var token = UseToken().token;
    
    var regData = 
    {
        eventId,
        circeId,
        userId,
        message,
        userName
    };
    var postBody = JSON.stringify(regData) 
    var datas = await fetch(`${Constants.GATEWAY_HOST}/ActivityService/Comments/AddComment`,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      },
      body:postBody
    })
    return datas;
  },
  async removeComment(Id)
  {
    var token = UseToken().token;
    var datas = await fetch(`${Constants.GATEWAY_HOST}/ActivityService/Comments/RemoveComment?id=${Id}`,{
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      }
    })
    return datas;
  }
}