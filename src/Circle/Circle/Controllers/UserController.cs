﻿using SKE.Circle.Services.Contracts;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SKE.Circle.Domain;
using SKE.Circle.Services.Repositories.Abstractions;
using Microsoft.AspNetCore.Authorization;

namespace SKE.Circle.Controllers
{
    /// <summary>
    /// контроллер пользователей в круге
    /// </summary>
    [Authorize()]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UserController
        : ControllerBase
    {
        private readonly IRepository<CircleInfo> _circleRepository;
        private IMapper _mapper;

        /// <summary>
        /// ctor контроллер пользователей в круге
        /// </summary>
        public UserController(IRepository<CircleInfo> circleRepository, IMapper mapper)
        {
            _circleRepository = circleRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// добавить пользователя в круг
        /// </summary>
        /// <param name="circleId">Id круга, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="user">email пользователя, например <example>user@test.com</example></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> AddUserToCircleAsync(Guid circleId, string user)
        {
            var circle = await _circleRepository.GetByIdAsync(circleId);

            if (circle == null)
                return NotFound();

            if (!String.IsNullOrEmpty(circle.Users.Where(x => x.Equals(user)).FirstOrDefault()))
            {
                return Ok();
            }

            circle.Users.Add(user);

            await _circleRepository.UpdateAsync(circle);

            return Ok();
        }

        /// <summary>
        /// Удалить пользователя из круга
        /// </summary>
        /// <param name="circleId">Id круга, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="user">email пользователя, например <example>user@test.com</example></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> RemoveUserToCircleAsync(Guid circleId, string user)
        {
            var circle = await _circleRepository.GetByIdAsync(circleId);

            if (circle == null)
                return NotFound();

            if (!String.IsNullOrEmpty(circle.Users.Where(x => x.Equals(user)).FirstOrDefault()))
            {
                circle.Users.Remove(user);
                await _circleRepository.UpdateAsync(circle);
            }

            return Ok();
        }
    }
}