﻿using Microsoft.EntityFrameworkCore;
using SKE.Core.Repositories.Implementations;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Repositories.Implementations
{
    public class TagRepository : Repository<Tag, Guid>, ITagRepository
    {
        public TagRepository(DataContext context) : base(context)
        {
        }

        public Task<List<Tag>> GetFitleredAsync()
        {
            return GetAll().ToListAsync();
        }
    }
}