﻿using AutoMapper;
using EMD = SKE.EventManager.Domain;
using KGD = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class CityMappings : Profile
    {
        public CityMappings()
        {
            CreateMap<KGD.City, EMD.City>();
            CreateMap<EMD.City, KGD.City>();
        }
    }
}
