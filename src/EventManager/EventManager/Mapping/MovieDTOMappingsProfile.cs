﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности фильм.
    /// </summary>
    public class MovieDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public MovieDTOMappingsProfile()
        {
            CreateMap<MovieDTO, EF.Movie>();
            CreateMap<EF.Movie, MovieDTO>();
        }
    }
}
