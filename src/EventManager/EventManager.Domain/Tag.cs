﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Теги
    /// </summary>
    public class Tag : BaseEntity
    {
        /// <summary>
        /// наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// связь тега и события
        /// </summary>
        public ICollection<TagToEvent>? TagToEvents { get; set; }
        /// <summary>
        /// событиия
        /// </summary>
        public ICollection<Event>? Events { get; set; }
        /// <summary>
        /// связь тега и места
        /// </summary>
        public ICollection<TagToPlace>? TagToPlaces { get; set; }
        /// <summary>
        /// места
        /// </summary>
        public ICollection<Place>? Places { get; set; }
    }
}
