﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace SKE.Feed.DataBaseContext
{
    public static class EntityFrameworkInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<DataContext>(optionsBuilder
                => optionsBuilder
                    //.UseLazyLoadingProxies() // lazy loading
                    .UseSqlite(connectionString));
                    //.UseSqlServer(connectionString));

                    //services.AddHealthChecks()
                    //    .AddDbContextCheck<UserActivityDataContext>();

            return services;
        }
    }
}