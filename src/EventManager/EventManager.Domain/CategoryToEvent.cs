﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Связь многие ко многим категории событий и события
    /// </summary>
    public class CategoryToEvent : BaseEntity
    {
        /// <summary>
        /// идентификатор категории события
        /// </summary>
        public Guid CategoryEventId { get; set; }
        /// <summary>
        /// категория события
        /// </summary>
        public CategoryEvent CategoryEvent { get; set; }

        /// <summary>
        /// идентификатор события
        /// </summary>
        public Guid EventId { get; set; }
        /// <summary>
        /// событие
        /// </summary>
        public Event Event { get; set; }
    }
}
