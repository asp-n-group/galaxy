﻿namespace SKE.CoreLib.Core
{
    /// <summary>
    /// Общая часть для всех моделей БД
    /// </summary>
    public abstract class BaseEntity : IEntity<Guid>
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
    }
}
