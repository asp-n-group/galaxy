# galaxy

[Стиль кода, CodeStyle](https://learn.microsoft.com/ru-ru/dotnet/csharp/fundamentals/coding-style/coding-conventions)

## Демо

https://vz5ep.draftium.site/

## Authors and acknowledgment
* Витальев Алексей a89045595245@gmail.com ДИПЛОМАТ
* Шунько Михаил mihail.shunko@yandex.ru реализатор
* Чернов Евгений chernov.e.i.31@gmail.com реализатор 
* Байков Алексей реализатор aford9880@gmail.com
* Павел Сидоров pvsidorov@gmail.com
* Потий Олег oleg.potiy@gmail.com

## License
For open source projects, say how it is licensed.


