﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SKE.Core.DTO.EventManager;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Controllers
{
    /// <summary>
    /// контроллер фильмов
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private IMovieService _service;
        private IMapper _mapper;
        private readonly ILogger<MovieController> _logger;

        /// <summary>
        /// ctor контроллер фильмов
        /// </summary>
        public MovieController(IMovieService service, IMapper mapper, ILogger<MovieController> logger)
        {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Получить данные фильмов согласно переданным фильтрам
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "GetMovies")]
        public async Task<ActionResult<IntegrationResponseDTO<MovieDTO>>> GetAsync([FromQuery] MovieFilterDTO movieFilterDTO)
        {
            _logger.LogInformation("Запрос фильмов");
            var result = await _service.GetMovies(movieFilterDTO);
            return Ok(
                new IntegrationResponseDTO<MovieDTO>
                {
                    Status = "Success",
                    Data = _mapper.Map<List<Movie>, List<MovieDTO>>(result.ToList())
                });
        }
    }
}
