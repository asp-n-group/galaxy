﻿namespace SKE.CoreLib.Core
{
    /// <summary>
    /// Общая часть для всех моделей БД
    /// </summary>
    public abstract class BaseEntityInt : IEntity<int>
    {
        public int Id { get; set; }
    }
}
