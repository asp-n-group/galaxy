﻿using System.Net;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SKE.Feed.Domain;
using SKE.Feed.Models;
using SKE.Feed.Services.Abstractions;
using SKE.Feed.Services.Contracts;

namespace SKE.Feed.Controllers
{
    /// <summary>
    /// Контроллер ленты
    /// </summary>
    
    [Route("api/[controller]")]
    [ApiController]
    public class FeedController : ControllerBase
    {
        private readonly IFeedService _service;
        private readonly ILogger<FeedController> _logger;
        private readonly IMapper _mapper;

        public FeedController(IFeedService service, ILogger<FeedController> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение ленты по id
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во элементов на странице</param>
        /// <returns>Модель ленты</returns>
        [Authorize()]
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetAuthAsync(int userId, int page = 0, int pageSize = 20)
        {
            var userPreferences = User.Claims.Where(x => x.Type == "UserPreference").Select(x => x.Value);
            var feed = await _service.GetByUserIdAsync(userId, userPreferences, page, pageSize);
            return Ok(_mapper.Map<FeedDTO, FeedModel>(feed));
        }

        /// <summary>
        /// Получение ленты без авторизации
        /// </summary>
        /// <param name="eventsId">Идентификаторы событий</param>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во элементов на странице</param>
        /// <returns>Модель ленты</returns>
        [AllowAnonymous]
        [HttpGet()]
        public async Task<IActionResult> GetAsync([FromQuery] QueryParams queryParams)
        {
            var feed = await _service.GetAsync(queryParams);
            return Ok(_mapper.Map<FeedDTO, FeedModel>(feed));
        }

        /// <summary>
        /// Создать ленту
        /// </summary>
        /// <param name="feedModel">Модель ленты</param>
        /// <returns>Идентификатор новой ленты</returns>
        [HttpPost]
        public async Task<IActionResult> Add(FeedModel feedModel)
        {
            return Ok(await _service.Create(_mapper.Map<FeedDTO>(feedModel)));
        }
    }
}
