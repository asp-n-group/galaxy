﻿using SKE.Feed.Services.Abstractions;

namespace SKE.Feed.Services.Implementations
{
    public class UserInterestsServiceStub : IUserInterestsService
    {
        public string CorrectInterest(string interest)
        {
            if (interest == "theater")
                return "Концерты";

            return interest;
        }

        public List<string> GetUserInterests(int userId)
        {
            var categories = new List<string>();
            switch (userId)
            {
                case 0:
                    {
                        categories.Add("Концерты");
                        break;
                    }
                case 1:
                    {
                        categories.Add("Спектакли");
                        break;
                    }
                case 2:
                    {
                        categories.Add("Обучение");
                        break;
                    }
                case 3:
                    {
                        categories.Add("Вечеринки");
                        break;
                    }
                case 4:
                    {
                        categories.Add("Выставки");
                        break;
                    }
                case 5:
                    {
                        categories.Add("Экскурсии");
                        break;
                    }
                case 6:
                    {
                        categories.Add("Фестивали");
                        break;
                    }
                case 7:
                    {
                        categories.Add("Кинопоказы");
                        break;
                    }
                case 8:
                    {
                        categories.Add("Мода и стиль");
                        break;
                    }
                case 9:
                    {
                        categories.Add("Праздники");
                        break;
                    }
                default:
                    {
                        categories.Add("Отдых");
                        break;
                    }
            }
            return categories;
        }
    }
}
