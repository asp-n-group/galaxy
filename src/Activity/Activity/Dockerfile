#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app


FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Activity/Activity/SKE.Activity.csproj", "Activity/Activity/"]
COPY ["Activity/Activity.DataBase/SKE.Activity.DataBase.csproj", "Activity/Activity.DataBase/"]
COPY ["Activity/Activity.Domain/SKE.Activity.Domain.csproj", "Activity/Activity.Domain/"]
COPY ["Core/CoreLib.Core/SKE.CoreLib.Core.csproj", "Core/CoreLib.Core/"]
COPY ["Activity/Activity.Repositories.Implementations/SKE.Activity.Repositories.Implementations.csproj", "Activity/Activity.Repositories.Implementations/"]
COPY ["Activity/Activity.Services.Repositories.Abstractions/SKE.Activity.Services.Repositories.Abstractions.csproj", "Activity/Activity.Services.Repositories.Abstractions/"]
COPY ["Activity/Activity.Services.Contracts/SKE.Activity.Services.Contracts.csproj", "Activity/Activity.Services.Contracts/"]
RUN dotnet restore "Activity/Activity/SKE.Activity.csproj"
COPY . .
WORKDIR "/src/Activity/Activity"
RUN dotnet build "SKE.Activity.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "SKE.Activity.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final



## FOR ADD SELF-SIGNED CERT TO TRUST STORE
RUN apt-get update -y && apt-get upgrade -y

# dotnet specific env vars, default to development environment

# dotnet kestrel env vars
ENV Kestrel:Certificates:Default:Path=/etc/ssl/private/cert.pfx
ENV Kestrel:Certificates:Default:Password=changeit
ENV Kestrel:Certificates:Default:AllowInvalid=true
ENV Kestrel:EndPointDefaults:Protocols=Http1AndHttp2

# copy certificate authority certs from local file system
ARG CA_KEY=./ske.asp-net-team.dev+3-key.pem
ARG CA_CERT=./ske.asp-net-team.dev+3.pem
ARG DOMAINS='localhost 127.0.0.1 ::1'

# default ca cert location (mkcert)
COPY ${CA_KEY} /root/.local/share/mkcert/rootCA-key.pem
COPY ${CA_CERT} /root/.local/share/mkcert/rootCA.pem

# install CA and SSL cert
RUN apt-get install curl -y && \
	curl -L https://github.com/FiloSottile/mkcert/releases/download/v1.4.3/mkcert-v1.4.3-linux-amd64 > /usr/local/bin/mkcert && \
	chmod +x /usr/local/bin/mkcert
RUN mkcert -install
RUN mkcert -p12-file /etc/ssl/private/cert.pfx -pkcs12 $DOMAINS

# Install locale
RUN apt-get install locales -y \
	&& localedef -f UTF-8 -i en_GB en_GB.UTF-8 \
	&& update-locale LANG=en_GB.utf8

ENV LANG=en_GB:en \
	LANGUAGE=en_GB:en \
	LC_ALL=en_GB.UTF-8


    
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SKE.Activity.dll"]