import * as React from 'react';
import { useState, useEffect } from 'react';
import getLocations from '../Services/Location';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Unstable_Grid2';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';

export default function Locations(props) {
    const [locations, setLocations] = useState([]);
    const [loaded, setLoaded] = useState(false);
  
    const [selectedLocation, selectLocation ] = useState(props.selectedLocation);
  
    useEffect(() => {
      
      if(loaded)
      {
        return;
      }
      setLoaded(true);
      const datap = async ()=>{
        var loc = await getLocations();
        
        setLocations(loc);
        selectLocation('');
      };
      datap();    
    },[locations, loaded]);
  
    const handleChange = (event) => {
      selectLocation(event.target.value);
      props.selectLocation(event.target.value)
    };

    return (
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid>
            <FormControl sx={{ m: 1, minWidth: 200 }}>
              <InputLabel sx={{ color: "white" }} id="label">Выберите город</InputLabel>
              <Select sx={{ color: "white" }} labelId="selectId" id="" value={props.selectedLocation}
                  label="Выберите город" onChange={handleChange}>
                <MenuItem key="" value="">
                  <em>-</em>
                </MenuItem>
                {locations.map((u, index) => (
                  <MenuItem key={u.slug} value={u.slug} >{u.name}</MenuItem> 
                ))} 
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      </Box>
    );
  }