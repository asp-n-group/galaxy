﻿using SKE.EmailNotificator.Domain;

namespace SKE.EmailNotifiicator.Services.Repositories.Abstractions
{
    public interface IEmailSendingService
    {
        public Task SendEmailAsync(SendEmailRequest emailRequest);
    }
}