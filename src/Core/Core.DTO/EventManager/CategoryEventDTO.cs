﻿namespace SKE.Core.DTO.EventManager
{
    public class CategoryEventDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// внешний идентификатор
        /// </summary>
        public int? ExternalId { get; set; }
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// название
        /// </summary>
        public string Name { get; set; }
    }
}
