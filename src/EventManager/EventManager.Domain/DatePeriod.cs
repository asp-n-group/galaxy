﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Временные периоды
    /// </summary>
    public class DatePeriod : BaseEntity
    {
        /// <summary>
        /// начало
        /// </summary>
        public DateTime Start { get; set; }
        /// <summary>
        /// окончание
        /// </summary>
        public DateTime End { get; set; }

        /// <summary>
        /// идентификатор события
        /// </summary>
        public Guid EventId { get; set; }
        /// <summary>
        /// событиее
        /// </summary>
        public Event Event { get; set; }
    }
}
