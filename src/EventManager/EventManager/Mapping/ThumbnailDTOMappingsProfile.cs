﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class ThumbnailDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public ThumbnailDTOMappingsProfile()
        {
            CreateMap<ThumbnailDTO, EF.Thumbnail>();
            CreateMap<EF.Thumbnail, ThumbnailDTO>();
        }
    }
}
