﻿using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Contracts;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Services.Implementations
{
    public class MovieService : IMovieService
    {
        IMovieRepository _movies;
        public MovieService(IMovieRepository movies)
        {
            _movies = movies;
        }

        public async Task<IEnumerable<Movie>> GetMovies(MovieFilterDTO movieFilterDTO)
        {
            return await _movies.GetFitleredAsync(movieFilterDTO);
        }
    }
}
