﻿using SKE.KudaGo.Domain.Model;

namespace SKE.KudaGo.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с фильмами
    /// </summary>
    public interface IMovieRepository : IRepository<KudaGoResponse<Movie>>
    {
        /// <summary>
        /// Запросить все сущности 
        /// </summary>
        /// <returns> массив сущностей</returns>
        IAsyncEnumerable<IEnumerable<Movie>> GetAllAsync();
    }
}