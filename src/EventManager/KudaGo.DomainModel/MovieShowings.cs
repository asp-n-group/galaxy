﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class MovieShowings
    {
        private uint _dateUnix;
        private DateTime _date;

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("movie")]
        public Movie Movie { get; set; }
        [JsonProperty("place")]
        public Place? Place { get; set; }
        [JsonProperty("datetime")]
        public uint DateTimeUnix
        {
            get { return _dateUnix; }
            set
            {
                _dateUnix = value;
                _date = new DateTime(1970, 1, 1)
                    .AddSeconds(_dateUnix);
            }
        }
        [JsonIgnore]
        public DateTime ShowingsDate { get { return _date; } set { _date = value; } }
        [JsonProperty("three_d")]
        public bool Is3D { get; set; }
        [JsonProperty("imax")]
        public bool IsImax { get; set; }
        [JsonProperty("four_dx")]
        public bool Is4D { get; set; }
        [JsonProperty("original_language")]
        public bool OriginalLanguage { get; set; }
        [JsonProperty("price")]
        public string Price { get; set; }
    }
}
