﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности категорий событий.
    /// </summary>
    public class CategoryEventDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public CategoryEventDTOMappingsProfile()
        {
            CreateMap<CategoryEventDTO, EF.CategoryEvent>();
            CreateMap<EF.CategoryEvent, CategoryEventDTO>();

            CreateMap<EF.CategoryEvent, CategoryEventGRPC>()
                .ForMember(x => x.Id, map => map.MapFrom(s => s.Id.ToString()));
        }
    }
}
