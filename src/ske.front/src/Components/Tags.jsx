import { useState, useEffect } from 'react';
import getPreferences from '../Services/Preferences';
import Grid from '@mui/material/Unstable_Grid2';
import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';




export default function Tags(props) {
  const [eventCategories, setEventCategories] = useState([]);
  const [loaded, setLoaded] = useState(false);

  const [selectedCat, selectCat ] = useState(props.selectedCat);
  useEffect(() => {
    selectCat(props.selectedCat)
    if(loaded)
    {
      return;
    }
    setLoaded(true);
    const datap = async ()=>{
      var categories = await getPreferences();
      setEventCategories(categories);
    };
    datap();    
  },[eventCategories, loaded]);

  const AddCategory = (slug) => () =>
  {
    if (!selectedCat.includes(slug)) {
      const copy = Array.from(selectedCat);
      copy.push(slug)
      selectCat(copy);
      props.setCat(copy);
    }
  };

  const RemoveCategory = (slug) => () =>
  {
    if (selectedCat.includes(slug)) {
      const copy = (Array.from(selectedCat)).filter(el => el !== slug);
      selectCat(copy);
      props.setCat(copy)
    }
  };

  return (
    <Box sx={{ width: '100%' }}>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        {eventCategories.map((item) => (
          <Grid key={item.slug}>
            <Chip  label={item.name} onClick={AddCategory(item.slug)} onDelete={!selectedCat.includes(item.slug) ? undefined : RemoveCategory(item.slug)} />
          </Grid>
        ))} 
      </Grid>
    </Box>
  );
}