﻿
using SKE.Core.Repositories.Abstractions;
using SKE.EventManager.Domain;

namespace SKE.EventManager.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с категориями мест
    /// </summary>
    public interface ICategoryPlaceRepository : IRepository<CategoryPlace, Guid>
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <returns> Список категорий мест. </returns>
        Task<List<CategoryPlace>> GetFitleredAsync();
    }
}
