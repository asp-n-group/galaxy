﻿using Microsoft.EntityFrameworkCore;
using SKE.Core.Repositories.Implementations;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Repositories.Implementations
{
    public class MovieShowingRepository : Repository<MovieShowings, Guid>, IMovieShowingRepository
    {
        public MovieShowingRepository(DataContext context) : base(context)
        {
        }
        public Task<List<MovieShowings>> GetFitleredAsync(MovieShowingFilterDTO filterDTO)
        {
            var result = GetAll();
            if (filterDTO.movieExternalId.HasValue)
            {
                result = result.Include(x => x.Movie)
                    .Where(x => x.Movie.ExternalId == filterDTO.movieExternalId.Value);
            }
            return result.ToListAsync();
        }
    }
}