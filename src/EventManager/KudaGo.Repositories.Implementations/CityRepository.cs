﻿using SKE.KudaGo.Domain.Model;
using SKE.KudaGo.Services.Repositories.Abstractions;

namespace SKE.KudaGo.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с городами
    /// </summary>
    public class CityRepository : Repository<IEnumerable<City>>, ICityRepository
    {
        private readonly string _urlPath = "locations/?lang=ru&fields=slug,name,timezone,coords,language,currency";

        /// <summary>
        /// Получить список городов
        /// </summary>
        /// <returns>список городов</returns>
        public async Task<IEnumerable<City>> GetAllAsync()
        {
            return await GetAllQueryAsync(_urlPath);
        }
    }
}
