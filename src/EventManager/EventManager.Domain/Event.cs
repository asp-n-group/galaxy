﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// События
    /// </summary>
    public class Event : BaseEntityExtension
    {
        /// <summary>
        /// дата публикации
        /// </summary>
        public DateTime PublicationDate { get; set; }
        /// <summary>
        /// название
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// описание
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// полное описание
        /// </summary>
        public string BodyText { get; set; }
        /// <summary>
        /// тэглайн
        /// </summary>
        public string TagLine { get; set; }
        /// <summary>
        /// возрастное ограничение
        /// </summary>
        public string? AgeRestriction { get; set; }
        /// <summary>
        /// стоимость
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// бесплатное ли событие
        /// </summary>
        public bool IsFree { get; set; }
        /// <summary>
        /// сколько пользователей добавило событие в избранное
        /// </summary>
        public int FavoritesCount { get; set; }
        /// <summary>
        /// число комментариев к событию
        /// </summary>
        public int CommentsCount { get; set; }
        /// <summary>
        /// короткое название
        /// </summary>
        public string ShortTitle { get; set; }

        /// <summary>
        /// идентификатор города проведения
        /// </summary>
        public Guid LocationId { get; set; }
        /// <summary>
        /// город проведения
        /// </summary>
        public City Location { get; set; }
        /// <summary>
        /// даты проведения
        /// </summary>
        public ICollection<DatePeriod> Dates { get; set; }
        /// <summary>
        /// связь тегов и событий
        /// </summary>
        public ICollection<TagToEvent> TagToEvents { get; set; }
        /// <summary>
        /// тэги события
        /// </summary>
        public ICollection<Tag> Tags { get; set; }
        /// <summary>
        /// связь категорий и событий
        /// </summary>
        public ICollection<CategoryToEvent> CategoryToEvents { get; set; }
        /// <summary>
        /// список категорий
        /// </summary>
        public ICollection<CategoryEvent> Categories { get; set; }
        /// <summary>
        /// картинки
        /// </summary>
        public ICollection<Image> Images { get; set; }
        /// <summary>
        /// идентификатор место проведения
        /// </summary>
        public Guid? PlaceId { get; set; }
        /// <summary>
        /// место проведения
        /// </summary>
        public Place? Place { get; set; }



        public Event()
        {
            Categories = new List<CategoryEvent>();
            Tags = new List<Tag>();
        }
    }
}
