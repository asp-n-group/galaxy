﻿using Microsoft.EntityFrameworkCore;
using SKE.Core.Repositories.Implementations;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Repositories.Implementations
{
    public class PlaceRepository : Repository<Place, Guid>, IPlaceRepository
    {
        private int _page = 1;
        private int _pageSize = 20;
        public PlaceRepository(DataContext context) : base(context)
        {
        }

        public Task<List<Place>> GetFitleredAsync(PlaceFilterDTO filterDTO)
        {
            var result = GetAll();
            if (filterDTO.IncludeCategories ?? false)
            {
                result = result.Include(x => x.Categories);
            }
            if (filterDTO.IncludeTags ?? false)
            {
                result = result.Include(x => x.Tags);
            }
            if (filterDTO.IncludeImages ?? false)
            {
                result = result.Include(x => x.Images)
                    .ThenInclude(x => x.Thumbnails);
            }

            if (filterDTO.Tags is not null)
            {
                result = result.Include(x => x.Tags)
                    .Where(x => x.Tags.Any(y => filterDTO.Tags.Contains(y.Name)));
            }
            if (filterDTO.Categories is not null)
            {
                result = result.Include(x => x.Categories)
                    .Where(x => x.Categories.Any(y => filterDTO.Categories.Contains(y.Name)));
            }

            if (filterDTO.CityId.HasValue)
            {
                result = result
                    .Where(x => x.LocationId == filterDTO.CityId);
            }

            if (filterDTO.PageSize.HasValue)
            {
                _pageSize = filterDTO.PageSize.Value <= 0 ? _pageSize : filterDTO.PageSize.Value;
            }
            if (filterDTO.Page.HasValue)
            {
                _page = filterDTO.Page.Value <= 0 ? _page : filterDTO.Page.Value;
            }

            return result
                .Take(_pageSize)
                .Skip((_page - 1) * _pageSize)
                .ToListAsync();
        }
    }
}