﻿namespace SKE.EventManager.Services.Contracts
{
    /// <summary>
    /// DTO Фильтра мест
    /// </summary>
    public class PlaceFilterDTO
    {
        public bool? IncludeCategories { get; set; } = false;
        public bool? IncludeTags { get; set; } = false;
        public bool? IncludeImages { get; set; } = false;

        public Guid? CityId { get; set; }
        public List<string>? Categories { get; set; }
        public List<string>? Tags { get; set; }

        public int? PageSize { get; set; } = 100;
        public int? Page { get; set; } = 1;
    }
}