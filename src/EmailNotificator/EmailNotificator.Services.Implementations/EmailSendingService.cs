﻿using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using SKE.EmailNotificator.Domain;
using SKE.EmailNotifiicator.Services.Repositories.Abstractions;
using Microsoft.Extensions.Configuration;

namespace SKE.EmailNotificator.Services.Implementations
{
    public class EmailSendingService : IEmailSendingService
    {
        private readonly string _serverEmail;
        private readonly string _smtpServer;
        private int _serverPort;
        private readonly string _password;
        public EmailSendingService(IConfiguration appConfig)
        {
            _serverEmail = appConfig["SmtpServerSettings:From"];
            _smtpServer = appConfig["SmtpServerSettings:SmtpServer"];
            int.TryParse(appConfig["SmtpServerSettings:Port"], out _serverPort);
            _password = appConfig["SmtpServerSettings:Password"];
        }

        public async Task SendEmailAsync(SendEmailRequest emailRequest)
        {
            var email = CreateMessage(emailRequest);
            using (var smtp = new SmtpClient())
            {
                smtp.Connect(_smtpServer, _serverPort, SecureSocketOptions.StartTls);
                smtp.Authenticate(_serverEmail, _password);
                await smtp.SendAsync(email);
                smtp.Disconnect(true);
            }
        }

        private MimeMessage CreateMessage(SendEmailRequest emailRequest)
        {
            var message = new MimeMessage();
            message.Sender = MailboxAddress.Parse(_serverEmail);
            message.To.Add(MailboxAddress.Parse(emailRequest.MailTo));
            message.Subject = emailRequest.Subject;
            var builder = new BodyBuilder();

            builder.HtmlBody = emailRequest.Message;
            message.Body = builder.ToMessageBody();
            return message;
        }
    }
}