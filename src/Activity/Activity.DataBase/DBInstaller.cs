﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using SKE.Activity.Domain;

namespace SKE.Activity.DataBase
{
    public static class DBInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddSingleton<IMongoClient>(_ =>
                    new MongoClient(connectionString))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .GetDatabase("activity_db"))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<Comment>("comment"))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<Likes>("likes"))
                .AddScoped(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .StartSession());
            return services;
        }
    }
}