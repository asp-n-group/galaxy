﻿
using SKE.Core.Repositories.Abstractions;
using SKE.EventManager.Domain;

namespace SKE.EventManager.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с категориями событий
    /// </summary>
    public interface ICategoryEventRepository : IRepository<CategoryEvent, Guid>
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <returns> Список категорий событий. </returns>
        Task<List<CategoryEvent>> GetFitleredAsync();
    }
}
