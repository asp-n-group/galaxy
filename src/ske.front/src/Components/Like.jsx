import * as React from 'react';
import { useEffect } from 'react';

import Box from '@mui/material/Box';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import LikeService from '../Services/LikeService';
import UserService from '../Services/UserService';

export default function Like (props) {
  const [userId, setUserId] = React.useState(UserService.getUserId()); 
  const [loaded, setLoaded] = React.useState(false);
  const [isLiked, setLike] = React.useState(false);
  
  useEffect(() => {    
    if(loaded)
    {
      return;
    }
    setLoaded(true);  
  },[loaded]);

  const addLike = async () => {
    await LikeService.addLike(props.itemDetails?.id, userId)
    datap();
  };

  const removeLike = async () => {
    await LikeService.removeLike(props.itemDetails?.id, userId)
    datap();
  };

  const datap = async ()=>{
    if(props.itemDetails?.id !== undefined && userId !== undefined)
    {
      var d = await LikeService.getLikeByUserEvent(props.itemDetails?.id, userId);
      setLike(d);
    }
  };

  if(UserService.IsAuthorized() === false)
  {
    return (<Box/>);
  }

  datap();

  if (isLiked !== true)
  {
    return(
      <IconButton aria-label="add to favorites" onClick={addLike}>
        <FavoriteBorderIcon />
      </IconButton>
    );
  }

  return(
    <IconButton aria-label="add to favorites" onClick={removeLike}>
      <FavoriteIcon />
    </IconButton>
  );
}