﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности событие.
    /// </summary>
    public class EventDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public EventDTOMappingsProfile()
        {
            CreateMap<EventDTO, EF.Event>();
            CreateMap<EF.Event, EventDTO>();

            CreateMap<EF.Event, EventItemGRPC>()
                .ForMember(x => x.PublicationDate, map => map.MapFrom(s => Google.Protobuf.WellKnownTypes.Timestamp.FromDateTime(DateTime.SpecifyKind(s.PublicationDate, DateTimeKind.Utc))))
                .ForAllMembers(
                    options => options.Condition((src, dest, srcValue) => srcValue != null));
        }
    }
}
