﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class Source
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("link")]
        public string Link { get; set; }
    }
}
