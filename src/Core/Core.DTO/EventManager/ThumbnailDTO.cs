﻿namespace SKE.Core.DTO.EventManager
{
    public class ThumbnailDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// ссылка на миниатюру 640*384
        /// </summary>
        public string Url640x384 { get; set; }
        /// <summary>
        /// ссылка на миниатюру 144*96
        /// </summary>
        public string Url144x96 { get; set; }

    }
}
