﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib;
using SKE.Feed.Domain;
using SKE.Feed.Services.Contracts;

namespace SKE.Feed.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности ленты
    /// </summary>
    public class FeedMappingsProfile : Profile
    {
        public FeedMappingsProfile()
        {
            CreateMap<FeedEvent, FeedEventDTO>();
            CreateMap<FeedEventDTO, FeedEvent>();

            CreateMap<EventDTO, FeedEvent>();
            CreateMap<FeedEvent, EventDTO>()
                .ForMember(x => x.Title, y => y.MapFrom(m => m.Title))
                .ForMember(x => x.Dates, y => y.MapFrom(m => m.Dates))
                .ForMember(x => x.IsFree, y => y.MapFrom(m => m.IsFree))
                .ForMember(x => x.BodyText, y => y.MapFrom(m => m.BodyText))
                .ForMember(x => x.ExternalId, y => y.Ignore())
                .ForMember(x => x.AgeRestriction, y => y.MapFrom(m => m.AgeRestriction))
                .ForMember(x => x.Categories, y => y.Ignore())
                .ForMember(x => x.CommentsCount, y => y.Ignore())
                .ForMember(x => x.FavoritesCount, y => y.Ignore())
                .ForMember(x => x.Id, y => y.MapFrom(m => m.Id))
                .ForMember(x => x.Images, y => y.MapFrom(m => m.Images))
                .ForMember(x => x.Categories, y => y.MapFrom(m => m.Categories))
                .ForMember(x => x.Location, y => y.MapFrom(m => m.Location))
                .ForMember(x => x.Place, y => y.MapFrom(m => m.Place))
                .ForMember(x => x.Price, y => y.MapFrom(m => m.Price))
                .ForMember(x => x.PublicationDate, y => y.MapFrom(m => m.PublicationDate))
                .ForMember(x => x.ShortTitle, y => y.MapFrom(m => m.ShortTitle))
                .ForMember(x => x.Slug, y => y.MapFrom(m => m.Slug))
                .ForMember(x => x.TagLine, y => y.MapFrom(m => m.TagLine))
                .ForMember(x => x.Description, y => y.MapFrom(m => m.Description))
                .ForMember(x => x.Tags, y => y.Ignore())
                ;

            CreateMap<Domain.Feed, FeedDTO>()
                .ForMember(x => x.Events, y => y.MapFrom(m => m.FeedEvents))
                ;

            CreateMap<FeedDTO, Domain.Feed>()
                .ForMember(x => x.FeedEvents, y => y.MapFrom(m => m.Events))
                .ForMember(x => x.Id, y => y.Ignore())
                ;

            CreateMap<EventItemGRPC, FeedEvent>()
                .ForMember(x => x.PublicationDate, map => map.MapFrom(s => s.PublicationDate.ToDateTime()));
        }
    }
}
