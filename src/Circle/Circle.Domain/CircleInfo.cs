﻿using SKE.CoreLib.Core;

namespace SKE.Circle.Domain
{
    /// <summary>
    /// Модель Круга
    /// </summary>
    public class CircleInfo : BaseEntity
    {
        /// <summary>
        /// имя круга
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// список пользователей в круге
        /// </summary>
        public ICollection<string> Users { get; set; } = new List<string>();

        /// <summary>
        /// список событиий в круге
        /// </summary>
        public ICollection<EventInfo> Events { get; set; } = new List<EventInfo>();
    }
}