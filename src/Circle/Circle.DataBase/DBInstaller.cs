﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using SKE.Circle.Domain;

namespace SKE.Circle.DataBase
{
    public static class DBInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
                    string connectionString)
        {
            services.AddSingleton<IMongoClient>(_ =>
                    new MongoClient(connectionString))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .GetDatabase("circle_db"))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<CircleInfo>("circle"))
                .AddScoped(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .StartSession());
            return services;
        }
    }
}