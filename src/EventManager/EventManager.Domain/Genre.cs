﻿
using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Жанры фильмов
    /// </summary>
    public class Genre : BaseEntityExtension
    {
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// связь жанра и фильма
        /// </summary>
        public ICollection<GenreToMovie> GenreToMovie { get; set; }
        /// <summary>
        /// фильмы
        /// </summary>
        public ICollection<Movie> Movies { get; set; }
    }
}
