﻿namespace SKE.Core.DTO.EventManager
{
    public class MovieDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// внешний идентификатор
        /// </summary>
        public int? ExternalId { get; set; }
        /// <summary>
        /// адрес фильма на сайте kudago.com
        /// </summary>
        public string SiteUrl { get; set; }
        /// <summary>
        /// дата публикации
        /// </summary>
        public DateTime PublicationDate { get; set; }
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// название
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// описание
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// полное описание
        /// </summary>
        public string BodyText { get; set; }
        /// <summary>
        /// является ли выбоором редакции
        /// </summary>
        public bool IsEditorsChoice { get; set; }
        /// <summary>
        /// число пользователей, добавивших фильм в избранное
        /// </summary>
        public int FavoritesCount { get; set; }
        /// <summary>
        /// число комментариев
        /// </summary>
        public int CommentsCount { get; set; }
        /// <summary>
        /// оригинальное название
        /// </summary>
        public string OriginalTitle { get; set; }
        /// <summary>
        /// язык оригинала
        /// </summary>
        public string Locale { get; set; }
        /// <summary>
        /// страна оригинала
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// год выпуска
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// язык оригинала
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// продолжительность
        /// </summary>
        public int? RunningTime { get; set; }
        /// <summary>
        /// бюджет (валюта)
        /// </summary>
        public string BudgetCurrency { get; set; }
        /// <summary>
        /// бюджет
        /// </summary>
        public decimal Budget { get; set; }
        /// <summary>
        /// рейтинг MPAA
        /// </summary>
        public string MpaaRating { get; set; }
        /// <summary>
        /// возрастное ограничение
        /// </summary>
        public string AgeRestriction { get; set; }
        /// <summary>
        /// актеры
        /// </summary>
        public string Stars { get; set; }
        /// <summary>
        /// режиссер
        /// </summary>
        public string Director { get; set; }
        /// <summary>
        /// сценарист
        /// </summary>
        public string Writer { get; set; }
        /// <summary>
        /// награды
        /// </summary>
        public string Awards { get; set; }
        /// <summary>
        /// трейлер
        /// </summary>
        public string Trailer { get; set; }
        /// <summary>
        /// сайт фильма
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// ссылка на страницу фильма на imdb.com
        /// </summary>
        public string ImdbUrl { get; set; }
        /// <summary>
        /// рейтинг фильма на сайте imdb.com
        /// </summary>
        public decimal? ImdbRating { get; set; }

        /// <summary>
        /// список жанров
        /// </summary>
        public ICollection<GenreDTO> Genres { get; set; }
        /// <summary>
        /// список показа
        /// </summary>
        public ICollection<MovieShowingsDTO> MovieShowings { get; set; }
        /// <summary>
        /// место показа
        /// </summary>
        public ICollection<PlaceDTO> Places { get; set; }
        /// <summary>
        /// галерея картинок
        /// </summary>
        public ICollection<ImageDTO> Images { get; set; }
        /// <summary>
        /// постер
        /// </summary>
        public ImageDTO Poster { get; set; }

        public MovieDTO()
        {
        }
    }
}
