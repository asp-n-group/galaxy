import * as React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TreeView from '@mui/lab/TreeView';
import TreeItem, { treeItemClasses } from '@mui/lab/TreeItem';
import Typography from '@mui/material/Typography';
import MailIcon from '@mui/icons-material/Mail';
import DeleteIcon from '@mui/icons-material/Delete';
import Label from '@mui/icons-material/Label';
import SupervisorAccountIcon from '@mui/icons-material/SupervisorAccount';
import InfoIcon from '@mui/icons-material/Info';
import ForumIcon from '@mui/icons-material/Forum';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import GroupIcon from '@mui/icons-material/Group';
import PeopleOutlineIcon from '@mui/icons-material/PeopleOutline';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import useToken from '../UseToken';
import { useState, useEffect } from 'react';
import UserService from '../Services/UserService';
import CircleService from '../Services/CircleService';
import FeedService from '../Services/FeedService';

const StyledTreeItemRoot = styled(TreeItem)(({ theme }) => ({
  color: theme.palette.text.secondary,
  [`& .${treeItemClasses.content}`]: {
    color: theme.palette.text.secondary,
    borderTopRightRadius: theme.spacing(2),
    borderBottomRightRadius: theme.spacing(2),
    paddingRight: theme.spacing(1),
    fontWeight: theme.typography.fontWeightMedium,
    '&.Mui-expanded': {
      fontWeight: theme.typography.fontWeightRegular,
    },
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
    },
    '&.Mui-focused, &.Mui-selected, &.Mui-selected.Mui-focused': {
      backgroundColor: `var(--tree-view-bg-color, ${theme.palette.action.selected})`,
      color: 'var(--tree-view-color)',
    },
    [`& .${treeItemClasses.label}`]: {
      fontWeight: 'inherit',
      color: 'inherit',
    },
  },
  [`& .${treeItemClasses.group}`]: {
    marginLeft: 0,
    [`& .${treeItemClasses.content}`]: {
      paddingLeft: theme.spacing(2),
    },
  },
}));


const handleMenuClick = (e) =>
{
  if(e.nodeId == 1) {
    e.props.setSelectCircle('')
    e.props.setEvents([])
    e.props.setChild(false)
  } else if(e.nodeId == 2) {
    window.location.href = '/create';
  } 
};

function StyledTreeItem(props) {
  const {
    bgColor,
    color,
    labelIcon: LabelIcon,
    labelInfo,
    labelText,
    nodeIdz,
    ...other
  } = props;

  return (
    <StyledTreeItemRoot onClick={(e)=>handleMenuClick(props)}
      label={
        <Box sx={{ display: 'flex', alignItems: 'center', p: 1, maxWidth:'80%', pr: 0 }}>
          <Box component={LabelIcon} color="inherit" sx={{ mr: 0 }} />
          <Typography variant="body2" sx={{ fontWeight: 'inherit', flexGrow: 1 , p:1}}>
            {labelText}
          </Typography>
          <Typography variant="caption" color="inherit">
            {labelInfo}
          </Typography>
        </Box>
      }
      style={{
        '--tree-view-color': color,
        '--tree-view-bg-color': bgColor,
      }}
      {...other}
    />
  );
}

StyledTreeItem.propTypes = {
  bgColor: PropTypes.string,
  color: PropTypes.string,
  labelIcon: PropTypes.elementType.isRequired,
  labelInfo: PropTypes.string,
  labelText: PropTypes.string.isRequired,
  nodeIdz: PropTypes.string,
};

export default function SkeTreeView(props) {
  const [circles, setCircles] = React.useState([]);
  const [loaded, setLoaded] = React.useState(false);
  const [selectCircle, setCircle] = React.useState();
 
  useEffect(() => {
    if(loaded)
    {
      return;
    }
    setLoaded(true);
    const datap = async ()=>{
      var d = await CircleService.getUserCircles();
      setCircles(d);
    };
    datap();    
  },[circles, loaded]);

  const handleChange = (event, nodeId) => {
    circles.forEach((treeRoot) => {
      if(treeRoot.id == nodeId && selectCircle != nodeId){
        setCircle(nodeId);
        let events = [];
        treeRoot.events.forEach((ev) => {
          events.push(ev.eventId);
        });
        props.setSelectCircle(nodeId)
        props.setEvents(events)
        props.setChild(true)
        return;
      }
    });
  };

  return (
    <TreeView aria-label="gmail" defaultCollapseIcon={<ArrowDropDownIcon />}
        defaultExpandIcon={<ArrowRightIcon />} defaultEndIcon={<div style={{ width: 10 }} />}
        onNodeSelect={handleChange} sx={{ flexGrow: 1  , m:1,}}>
      <StyledTreeItem key={1} nodeId="1" labelText="Лента" labelIcon={CalendarMonthIcon} props={props}/>
      <StyledTreeItem key={2} nodeId="2" labelText="Создать круг" labelIcon={GroupAddIcon} props={props}/>
      <StyledTreeItem key={3} nodeId="3" labelText="Мои круги" labelIcon={GroupIcon} >
        {circles.map((u, index) => (
          <StyledTreeItem key={u.id} nodeId={u.id} labelText={u.name} labelIcon={PeopleOutlineIcon}
              color="#1a73e8" bgColor="#e8f0fe">
            {u.users.map((uuid, index) => (
              <StyledTreeItem key={uuid} nodeId={uuid} labelText={uuid} labelIcon={PeopleOutlineIcon} 
                color="#1a73e8" bgColor="#e8f0fe" >
              </StyledTreeItem>
            ))} 
          </StyledTreeItem>
        ))} 
      </StyledTreeItem>
    </TreeView>
  );
}