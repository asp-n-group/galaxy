﻿using SKE.KudaGo.Domain.Model;

namespace SKE.KudaGo.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с категориями событий
    /// </summary>
    public interface ICategoryEventRepository : IRepository<IEnumerable<CategoryEvent>>
    {
        /// <summary>
        /// Запросить все сущности 
        /// </summary>
        /// <returns> массив сущностей</returns>
        Task<IEnumerable<CategoryEvent>> GetAllAsync();
    }
}