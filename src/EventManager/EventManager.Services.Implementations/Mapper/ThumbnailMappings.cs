﻿using AutoMapper;
using EF = SKE.EventManager.Domain;
using KG = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности миниатюр.
    /// </summary>
    public class ThumbnailMappings : Profile
    {
        public ThumbnailMappings()
        {
            CreateMap<KG.Thumbnail, EF.Thumbnail>();
        }
    }
}
