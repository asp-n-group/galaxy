﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class TagDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public TagDTOMappingsProfile()
        {
            CreateMap<TagDTO, EF.Tag>();
            CreateMap<EF.Tag, TagDTO>();

            CreateMap<EF.Tag, TagGRPC>()
                .ForMember(x => x.Id, map => map.MapFrom(s => s.Id.ToString()));
        }
    }
}
