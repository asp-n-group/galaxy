﻿
using SKE.Core.Repositories.Abstractions;
using SKE.EventManager.Domain;

namespace SKE.EventManager.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с тегами
    /// </summary>
    public interface ITagRepository : IRepository<Tag, Guid>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <returns> Список тегов. </returns>
        Task<List<Tag>> GetFitleredAsync();
    }
}
