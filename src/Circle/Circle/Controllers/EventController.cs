﻿using SKE.Circle.Services.Contracts;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SKE.Circle.Domain;
using SKE.Circle.Services.Repositories.Abstractions;
using Microsoft.AspNetCore.Authorization;

namespace SKE.Circle.Controllers
{
    /// <summary>
    /// контроллер событий
    /// </summary>
    [Authorize()]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EventController
        : ControllerBase
    {
        private readonly IRepository<CircleInfo> _circleRepository;
        private IMapper _mapper;

        /// <summary>
        /// ctor контроллер событий
        /// </summary>
        public EventController(IRepository<CircleInfo> circleRepository, IMapper mapper)
        {
            _circleRepository = circleRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// добавить событие в круг
        /// </summary>
        /// <param name="circleId">Id круга, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="eventId">Id события, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> AddEventToCircleAsync(Guid circleId, Guid eventId)
        {
            var circle = await _circleRepository.GetByIdAsync(circleId);

            if (circle == null)
                return NotFound();

            if (circle.Events.Where(x => x.EventId.Equals(eventId)).FirstOrDefault() is not null)
            {
                return Ok();
            }

            circle.Events.Add(new EventInfo { EventId = eventId, DateAdd = DateTime.Now });

            await _circleRepository.UpdateAsync(circle);

            return Ok();
        }

        /// <summary>
        /// Удалить событие из круга
        /// </summary>
        /// <param name="circleId">Id круга, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="eventId">Id события, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpDelete]

        public async Task<IActionResult> RemoveUserToCircleAsync(Guid circleId, Guid eventId)
        {
            var circle = await _circleRepository.GetByIdAsync(circleId);

            if (circle == null)
                return NotFound();

            var eventExist = circle.Events.Where(x => x.EventId.Equals(eventId)).FirstOrDefault();
            if (eventExist is not null)
            {
                circle.Events.Remove(eventExist);
                await _circleRepository.UpdateAsync(circle);
            }

            return Ok();
        }
    }
}