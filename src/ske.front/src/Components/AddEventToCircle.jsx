import * as React from 'react';
import { useEffect } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Unstable_Grid2';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import CircleService from '../Services/CircleService';
import UserService from '../Services/UserService';



export default function AddEventToCircle (props) {
  const [circles, setCircles] = React.useState([]);
  const [selectCircle, setCircle] = React.useState('');
  const [loaded, setLoaded] = React.useState(false);

  useEffect(() => {
    if(loaded)
    {
      return;
    }
    setLoaded(true);
    const datap = async ()=>{
      var d = await CircleService.getUserCircles();
      setCircles(d);
    };
    datap();
  },[circles, loaded]);
  

  const handleChange = (event) => {
    setCircle(event.target.value);
  };
  
  const addToCircle = async () => {
    if( selectCircle !== '' && props.itemDetails?.id !== '')
    {
      var result = await CircleService.addEventToCircle(selectCircle, props.itemDetails?.id) ;
    }
  }

  if(UserService.IsAuthorized() === false)
  {
    return (<Box/>);
  }
  
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid >
          <FormControl sx={{ m: 1, minWidth: 200 }}>
            <InputLabel id="label">Выберите круг</InputLabel>
            <Select labelId="selectId" id="" value={selectCircle}
                label="Выберите круг" onChange={handleChange}>
              <MenuItem key="" value="">
                <em>-</em>
              </MenuItem>
              {circles.map((u, index) => (
                <MenuItem key={u.id} value={u.id} >{u.name}</MenuItem> 
              ))} 
            </Select>
          </FormControl>
        </Grid>
        <Grid display="flex" alignItems="center" >
          <Button variant="contained" size="large" color="primary" onClick={addToCircle} >Добавить</Button>
        </Grid>
      </Grid>
    </Box>
  );
}