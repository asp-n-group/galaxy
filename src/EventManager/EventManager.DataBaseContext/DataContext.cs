﻿using Microsoft.EntityFrameworkCore;
using SKE.EventManager.Domain;

namespace SKE.EventManager.DataBaseContext
{
    public class DataContext : DbContext
    {
        public DbSet<CategoryEvent> CategoryEvents { get; set; }
        public DbSet<CategoryPlace> CategoryPlaces { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Coordinate> Coordinates { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<DatePeriod> DatePeriods { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<Thumbnail> Thumbnails { get; set; }


        public DataContext(DbContextOptions<DataContext> options)
                            : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            Database.MigrateAsync().GetAwaiter().GetResult();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CategoryEvent>()
                .HasMany(x => x.Events)
                .WithMany(x => x.Categories)
                .UsingEntity<CategoryToEvent>(
                    x => x.HasOne(y => y.Event)
                        .WithMany(y => y.CategoryToEvents)
                        .HasForeignKey(y => y.EventId),
                    x => x.HasOne(y => y.CategoryEvent)
                        .WithMany(y => y.CategoryToEvents)
                        .HasForeignKey(y => y.CategoryEventId),
                    x =>
                    {
                        x.HasKey(t => new { t.EventId, t.CategoryEventId });
                        //x.ToTable("CategoryToEvent");
                    });
            modelBuilder.Entity<CategoryPlace>()
                .HasMany(x => x.Places)
                .WithMany(x => x.Categories)
                .UsingEntity<CategoryToPlace>(
                    x => x.HasOne(y => y.Place)
                        .WithMany(y => y.CategoryToPlaces)
                        .HasForeignKey(y => y.PlaceId),
                    x => x.HasOne(y => y.CategoryPlace)
                        .WithMany(y => y.CategoryToPlaces)
                        .HasForeignKey(y => y.CategoryPlaceId),
                    x =>
                    {
                        x.HasKey(t => new { t.PlaceId, t.CategoryPlaceId });
                        //x.ToTable("CategoryToPlaces");
                    });
            modelBuilder.Entity<City>()
                .HasMany(x => x.Places)
                .WithOne(x => x.Location)
                .HasForeignKey(x => x.LocationId);
            modelBuilder.Entity<City>()
                .HasMany(x => x.Events)
                .WithOne(x => x.Location)
                .HasForeignKey(x => x.LocationId);
            modelBuilder.Entity<City>()
                .HasOne(x => x.Coords)
                .WithOne(x => x.City)
                .HasForeignKey<Coordinate>(x => x.CityId);
            modelBuilder.Entity<Place>()
                .HasOne(x => x.Coords)
                .WithOne(x => x.Place)
                .HasForeignKey<Coordinate>(x => x.PlaceId);
            modelBuilder.Entity<Event>()
                .HasMany(x => x.Dates)
                .WithOne(x => x.Event)
                .HasForeignKey(x => x.EventId);
            modelBuilder.Entity<Tag>()
                 .HasMany(x => x.Events)
                 .WithMany(x => x.Tags)
                 .UsingEntity<TagToEvent>(
                     x => x.HasOne(y => y.Event)
                         .WithMany(y => y.TagToEvents)
                         .HasForeignKey(y => y.EventId),
                     x => x.HasOne(y => y.Tag)
                         .WithMany(y => y.TagToEvents)
                         .HasForeignKey(y => y.TagId),
                     x =>
                     {
                         x.HasKey(t => new { t.EventId, t.TagId });
                         //x.ToTable("TagToEvent");
                     });
            modelBuilder.Entity<Tag>()
                 .HasMany(x => x.Places)
                 .WithMany(x => x.Tags)
                 .UsingEntity<TagToPlace>(
                     x => x.HasOne(y => y.Place)
                         .WithMany(y => y.TagToPlaces)
                         .HasForeignKey(y => y.PlaceId),
                     x => x.HasOne(y => y.Tag)
                         .WithMany(y => y.TagToPlaces)
                         .HasForeignKey(y => y.TagId),
                     x =>
                     {
                         x.HasKey(t => new { t.PlaceId, t.TagId });
                         //x.ToTable("TagToPlace");
                     });
            modelBuilder.Entity<Event>()
                .HasMany(x => x.Images)
                .WithOne(x => x.Event)
                .HasForeignKey(x => x.EventId);
            modelBuilder.Entity<Event>()
                .HasMany(x => x.Dates)
                .WithOne(x => x.Event)
                .HasForeignKey(x => x.EventId);
            modelBuilder.Entity<Place>()
                .HasMany(x => x.Images)
                .WithOne(x => x.Place)
                .HasForeignKey(x => x.PlaceId);
            modelBuilder.Entity<Place>()
                .HasMany(x => x.Events)
                .WithOne(x => x.Place)
                .HasForeignKey(x => x.PlaceId);
            modelBuilder.Entity<Image>()
                .HasOne(x => x.Thumbnails)
                .WithOne(x => x.Image)
                .HasForeignKey<Thumbnail>(x => x.ImageId);
            modelBuilder.Entity<Image>()
                .HasOne(x => x.Source)
                .WithOne(x => x.Image)
                .HasForeignKey<Source>(x => x.ImageId);
            modelBuilder.Entity<Genre>()
                .HasMany(x => x.Movies)
                .WithMany(x => x.Genres)
                .UsingEntity<GenreToMovie>(
                    x => x.HasOne(y => y.Movie)
                        .WithMany(y => y.GenreToMovie)
                        .HasForeignKey(y => y.MovieId),
                    x => x.HasOne(y => y.Genre)
                        .WithMany(y => y.GenreToMovie)
                        .HasForeignKey(y => y.GenreId),
                    x =>
                    {
                        x.HasKey(t => new { t.MovieId, t.GenreId });
                    });
            modelBuilder.Entity<Movie>()
                .HasMany(x => x.Places)
                .WithMany(x => x.Movies)
                .UsingEntity<MovieShowings>(
                    x => x.HasOne(y => y.Place)
                        .WithMany(y => y.MovieShowings)
                        .HasForeignKey(y => y.PlaceId),
                    x => x.HasOne(y => y.Movie)
                        .WithMany(y => y.MovieShowings)
                        .HasForeignKey(y => y.MovieId)
                    );
            modelBuilder.Entity<Movie>()
                .HasMany(x => x.Images)
                .WithOne(x => x.Movie)
                .HasForeignKey(x => x.MovieId);
            modelBuilder.Entity<Movie>()
                .HasOne(x => x.Poster)
                .WithOne(x => x.MoviePoster)
                .HasForeignKey<Image>(x => x.MoviePosterId);
        }
    }
}