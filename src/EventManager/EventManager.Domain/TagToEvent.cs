﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Связь многие ко многим теги и события
    /// </summary>
    public class TagToEvent : BaseEntity
    {
        /// <summary>
        /// идентификатор тега 
        /// </summary>
        public Guid TagId { get; set; }
        /// <summary>
        /// тег
        /// </summary>
        public Tag Tag { get; set; }

        /// <summary>
        /// идентификатор события
        /// </summary>
        public Guid EventId { get; set; }
        /// <summary>
        /// событие
        /// </summary>
        public Event Event { get; set; }
    }
}
