import * as React from 'react';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import StarIcon from '@mui/icons-material/Star';

const labels: { [index: string]: string } = {
  0.5: 'Вообще никак',
  1: 'Никак',
  1.5: 'Очень плохо',
  2: 'Плохо',
  2.5: 'Хорошо',
  3: 'Очень хорошо',
  3.5: 'Отлично',
  4: 'Прелестно',
  4.5: 'Замечательно',
  5: 'Обварожительно',
};

export default function StartsRating() {
  function getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

  const vals = [0.5,1,1.5,2,2.5,3,3.5,4,4.5,5];
  const value = vals[getRandomInt(9)];

  return (
    <Box
      sx={{
        width: 300,
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <Rating
        name="text-feedback"
        value={value}
        readOnly
        precision={0.5}
        emptyIcon={<StarIcon style={{ opacity: 0.55 }} fontSize="inherit" />}
      />
      <Box sx={{ ml: 2 }}>{labels[value]}</Box>
    </Box>
  );
}