﻿namespace SKE.Core.DTO.EventManager
{
    public class ImageDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// ссылка на изображение 
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// миниатюры 
        /// </summary>
        public ThumbnailDTO Thumbnails { get; set; }
        /// <summary>
        /// источник
        /// </summary>
        public SourceDTO Source { get; set; }
    }
}
