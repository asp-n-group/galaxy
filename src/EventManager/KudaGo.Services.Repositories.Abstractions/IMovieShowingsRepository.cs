﻿using SKE.KudaGo.Domain.Model;

namespace SKE.KudaGo.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с показами фильмов
    /// </summary>
    public interface IMovieShowingsRepository : IRepository<KudaGoResponse<MovieShowings>>
    {
        /// <summary>
        /// Запросить все сущности 
        /// </summary>
        /// <returns> массив сущностей</returns>
        IAsyncEnumerable<IEnumerable<MovieShowings>> GetAllAsync(int movieId);
    }
}