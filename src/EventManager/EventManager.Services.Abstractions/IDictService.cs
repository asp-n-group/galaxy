﻿using SKE.EventManager.Domain;

namespace SKE.EventManager.Services.Abstractions
{
    public interface IDictService
    {
        Task<IEnumerable<CategoryEvent>> GetCategoryEvents();
        Task<IEnumerable<CategoryPlace>> GetCategoryPlaces();
        Task<IEnumerable<City>> GetCities();
        Task<IEnumerable<Genre>> GetGenres();
        Task<IEnumerable<Tag>> GetTags();
    }
}
