﻿using AutoMapper;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Mapping;
using SKE.EventManager.Services;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Implementations;
using System.Reflection;
using EM = SKE.EventManager.Services.Repositories.Abstractions;
using EMI = SKE.EventManager.Repositories.Implementations;
using KG = SKE.KudaGo.Services.Repositories.Abstractions;
using KGI = SKE.KudaGo.Repositories.Implementations;
using MapIntegration = SKE.EventManager.Services.Implementations.Mapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using SKE.CoreLib.Core;

namespace SKE.EventManager
{
    /// <summary>
    /// расширение служб
    /// </summary>
    public static class RegistrationServices
    {
        /// <summary>
        /// добавление служб
        /// </summary>
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddCorsReg(configuration);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = configuration["Jwt:Host"],
                        ValidateAudience = true,
                        ValidAudience = configuration["Jwt:Host"],
                        ValidateLifetime = true,
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(configuration["Jwt:Key"])),
                        ValidateIssuerSigningKey = true,
                    };
                });
            services.AddAuthorization();
            services.InstallDatabase(configuration);
            services.InstallAutomaper();
            services.InstallServices();
            services.InstallRepositories();
            services.InstallRedis(configuration);
            services.InstallgRPC();
            services.InstallSwagger();
            services.InstallHangfire(configuration);
            return services;
        }

        /// <summary>
        /// добавление логирования
        /// </summary>
        public static IHostBuilder AddHosts(this IHostBuilder services, IConfiguration config)
        {
            services.UseSerilog((context, services, configuration) =>
                configuration
                .ReadFrom.Configuration(context.Configuration)
                .Enrich.FromLogContext()
                .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(config.GetConnectionString("Elastic").ToString()))
                {
                    FailureCallback = e =>
                    {
                        Console.WriteLine("Unable to submit event " + e.Exception);
                    },
                    TypeName = null,
                    IndexFormat = "EventManager-Log",
                    AutoRegisterTemplate = true,
                    EmitEventFailure = EmitEventFailureHandling.ThrowException | EmitEventFailureHandling.RaiseCallback | EmitEventFailureHandling.WriteToSelfLog
                })
            );
            return services;
        }

        /// <summary>
        /// добавление gRPC
        /// </summary>
        public static IEndpointRouteBuilder AddEndpoints(this IEndpointRouteBuilder endpoint)
        {
            endpoint.MapGrpcService<DictionaryApiService>();
            endpoint.MapGrpcService<EventApiService>();
            return endpoint;
        }

        private static IServiceCollection InstallDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureContext(configuration.GetConnectionString("DefaultConnection"));
            services.AddDbContext<HangFireContext>(x =>
            {
                x.UseNpgsql(configuration.GetConnectionString("HangFireConnection"));
            });
            return services;
        }

        private static IServiceCollection InstallRedis(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDistributedMemoryCache();
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = configuration.GetConnectionString("Redis");
            });
            return services;
        }

        private static IServiceCollection InstallgRPC(this IServiceCollection services)
        {
            services.AddGrpc();
            return services;
        }

        private static IServiceCollection InstallSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API Documentation",
                    Version = "v1"
                });
                var entryAssembly = Assembly.GetEntryAssembly();
                if (entryAssembly != null)
                {
                    var xmlFile = Path.ChangeExtension(entryAssembly.Location, ".xml");
                    c.IncludeXmlComments(xmlFile);
                }

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] {}
                }});
            });
            return services;
        }

        private static IServiceCollection InstallServices(this IServiceCollection services)
        {
            services
                .AddTransient<ISyncService, SyncService>()
                .AddTransient<IDictService, DictService>()
                .AddTransient<IEventService, EventService>()
                .AddTransient<IMovieService, MovieService>()
                .AddTransient<IPlaceService, PlaceService>();
            //    .AddTransient<IBankPaymentService, BankPaymentService>()
            //    .AddTransient<IBankReceiptService, BankReceiptService>()
            //    .AddTransient<IBankTransactionService, BankTransactionService>();
            return services;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection services)
        {
            services
                .AddTransient<EM.ICategoryEventRepository, EMI.CategoryEventRepository>()
                .AddTransient<EM.ICategoryPlaceRepository, EMI.CategoryPlaceRepository>()
                .AddTransient<EM.ICityRepository, EMI.CityRepository>()
                .AddTransient<EM.IEventRepository, EMI.EventRepository>()
                .AddTransient<EM.IGenreRepository, EMI.GenreRepository>()
                .AddTransient<EM.IMovieRepository, EMI.MovieRepository>()
                .AddTransient<EM.IMovieShowingRepository, EMI.MovieShowingRepository>()
                .AddTransient<EM.IPlaceRepository, EMI.PlaceRepository>()
                .AddTransient<EM.ITagRepository, EMI.TagRepository>();
            services
                .AddTransient<KG.ICategoryEventRepository, KGI.CategoryEventRepository>()
                .AddTransient<KG.ICategoryPlaceRepository, KGI.CategoryPlaceRepository>()
                .AddTransient<KG.ICityRepository, KGI.CityRepository>()
                .AddTransient<KG.IEventRepository, KGI.EventRepository>()
                .AddTransient<KG.IMovieRepository, KGI.MovieRepository>()
                .AddTransient<KG.IMovieShowingsRepository, KGI.MovieShowingsRepository>()
                .AddTransient<KG.IPlaceRepository, KGI.PlaceRepository>();
            return services;
        }

        private static IServiceCollection InstallAutomaper(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<MapIntegration.CategoryEventMappings>();
                    cfg.AddProfile<MapIntegration.CategoryPlaceMappings>();
                    cfg.AddProfile<MapIntegration.CityMappings>();
                    cfg.AddProfile<MapIntegration.CoordinateMappings>();
                    cfg.AddProfile<MapIntegration.DatesMappings>();
                    cfg.AddProfile<MapIntegration.EventMappings>();
                    cfg.AddProfile<MapIntegration.GenreMappings>();
                    cfg.AddProfile<MapIntegration.ImageMappings>();
                    cfg.AddProfile<MapIntegration.MovieMappings>();
                    cfg.AddProfile<MapIntegration.MovieShowingMappings>();
                    cfg.AddProfile<MapIntegration.PlaceMappings>();
                    cfg.AddProfile<MapIntegration.SourceMappings>();
                    cfg.AddProfile<MapIntegration.ThumbnailMappings>();
                    cfg.AddProfile<CategoryEventDTOMappingsProfile>();
                    cfg.AddProfile<CategoryPlaceDTOMappingsProfile>();
                    cfg.AddProfile<CityDTOMappingsProfile>();
                    cfg.AddProfile<TagDTOMappingsProfile>();
                    cfg.AddProfile<GenreDTOMappingsProfile>();
                    cfg.AddProfile<EventDTOMappingsProfile>();
                    cfg.AddProfile<MovieDTOMappingsProfile>();
                    cfg.AddProfile<ImageDTOMappingsProfile>();
                    cfg.AddProfile<SourceDTOMappingsProfile>();
                    cfg.AddProfile<ThumbnailDTOMappingsProfile>();
                    cfg.AddProfile<PlaceDTOMappingsProfile>();
                    cfg.AddProfile<DatesDTOMappingsProfile>();
                    cfg.AddProfile<EventFilterDTOMappingsProfile>();
                });
            //configuration.AssertConfigurationIsValid();

            services.AddSingleton<IMapper>(new Mapper(configuration));
            return services;
        }

        private static IServiceCollection InstallHangfire(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHangfire(c =>
                c.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                    .UseSimpleAssemblyNameTypeSerializer()
                    .UseRecommendedSerializerSettings()
                    .UsePostgreSqlStorage(configuration.GetConnectionString("HangFireConnection"),
                        new PostgreSqlStorageOptions
                        {
                            //AllowUnsafeValues = true,
                            QueuePollInterval = TimeSpan.FromSeconds(5)
                        }
)
                    );
            services.AddHangfireServer(optionsAction =>
            {
                optionsAction.ServerName = String.Format("{0}.{1}", Environment.MachineName, "TEST");
                optionsAction.Queues = new string[] { "eventmanager" };
                optionsAction.ServerTimeout = TimeSpan.FromDays(1);
            });

            return services;
        }
    }
}