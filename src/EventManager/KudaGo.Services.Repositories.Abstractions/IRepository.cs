﻿namespace SKE.KudaGo.Services.Repositories.Abstractions
{
    /// <summary>
    /// Базовый интерфейс всех репозиториев
    /// </summary>
    public interface IRepository
    {
    }

    /// <summary>
    /// Интерфейс репозитория, предназначенного для чтения
    /// </summary>
    /// <typeparam name="T">Тип Entity для репозитория</typeparam>
    public interface IRepository<T> : IRepository
    {
        /// <summary>
        /// Запросить все сущности 
        /// </summary>
        /// <param name="path">параметры запроса</param>
        /// <returns> сущность</returns>
        Task<T> GetAllQueryAsync(string path);
    }
}