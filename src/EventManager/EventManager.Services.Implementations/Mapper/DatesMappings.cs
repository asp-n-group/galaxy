﻿using AutoMapper;
using EF = SKE.EventManager.Domain;
using KG = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности время проведения.
    /// </summary>
    public class DatesMappings : Profile
    {
        public DatesMappings()
        {
            CreateMap<KG.DatePeriod, EF.DatePeriod>();
        }
    }
}
