﻿
using SKE.Core.Repositories.Abstractions;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы c событиями
    /// </summary>
    public interface IEventRepository : IRepository<Event, Guid>
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <param name="filterDTO"> ДТО фильтра событий. </param>
        /// <returns> Список событий. </returns>
        Task<List<Event>> GetFitleredAsync(EventFilterDTO filterDTO);
    }
}
