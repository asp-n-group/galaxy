﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class DatePeriod
    {
        private long _dateStartUnix;
        private long _dateEndUnix;
        private DateTime _dateStart;
        private DateTime _dateEnd;

        [JsonProperty("start")]
        public long StartUnix
        {
            get { return _dateStartUnix; }
            set
            {
                _dateStartUnix = value;
                _dateStart = new DateTime(1970, 1, 1)
                    .AddSeconds(_dateStartUnix);
            }
        }
        [JsonIgnore]
        public DateTime Start { get { return _dateStart; } set { _dateStart = value; } }

        [JsonProperty("end")]
        public long EndUnix
        {
            get { return _dateEndUnix; }
            set
            {
                _dateEndUnix = value;
                _dateEnd = new DateTime(1970, 1, 1)
                    .AddSeconds(_dateEndUnix);
            }
        }
        [JsonIgnore]
        public DateTime End { get { return _dateEnd; } set { _dateEnd = value; } }
    }
}
