﻿using SKE.CoreLib.Core;

namespace SKE.Activity.Domain
{
    /// <summary>
    /// Модель лайка
    /// </summary>
    public class Likes : BaseEntity
    {
        /// <summary>
        /// ссылка на событие
        /// </summary>
        public Guid EventId { get; set; }
        /// <summary>
        /// ссылка на пользователя
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// дата время установки
        /// </summary>
        public DateTime PublicationDatetime { get; set; }
        /// <summary>
        /// тим лайка, строковое значение
        /// </summary>
        public bool isLike { get; set; } = false;
    }
}