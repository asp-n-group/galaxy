import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import { Typography } from '@mui/material';
import { alignProperty } from '@mui/material/styles/cssUtils';
import dayjs, { Dayjs } from 'dayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { useState } from 'react';
import Chip from '@mui/material/Chip';
import Autocomplete from '@mui/material/Autocomplete';
import { useEffect } from 'react';
import Constants from '../Constants';
import getPreferences from '../Services/Preferences';
import getLocations from '../Services/Location';
import IdentityService from '../Services/IdentityService';

export default function Registration() {
 
  const [email, setEmail] = useState();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [dateOfBirdthUser, setDateOfBirdthUser] = useState(dayjs(new Date()));
  const [password, setPassword] = useState();
  const [passwordConfirmUser, setPasswordConfirm] = useState();
  const [preferences, setPreferences] = useState();
  const [preferencesPlatform, setPreferencesPlatform] = useState([]);

  const [location, setLocation] = useState();
  const [locationPlatform, setLocationPlatform] = useState([]);

  const [emailError, setEmailError] = useState(false);
  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [dateOfBirdthUserError, setDateOfBirdthUserError] = useState(false); 
  const [passwordConfirmUserError, setPasswordConfirmUserError] = useState(false);
  const [preferencesError, setPreferencesError] = useState(false);
  const [locationError, setLocationError] = useState(false);

  const [emailErrorText, setEmailErrorText] = useState();
  const [firstNameErrorText, setFirstNameErrorText] = useState();
  const [lastNameErrorText, setLastNameErrorText] = useState();
  const [dateOfBirdthUserErrorText, setDateOfBirdthUserErrorText] = useState(); 
  const [passwordConfirmUserErrorText, setPasswordConfirmUserErrorText] = useState();
  const [preferencesErrorText, setPreferencesErrorText] = useState();
  const [locationErrorText, setLocationErrorText] = useState();

  const [loaded, setLoaded] = useState(0);
  const [value, setValue] = React.useState(dayjs(new Date()));
  
  const handleSubmit = async e => {
    e.preventDefault();
    if(password !== passwordConfirmUser)
    {
      setPasswordConfirmUserError(true)
      setPasswordConfirmUserErrorText('Пароли не совпадают');
      return;
    }
    const gender = 'gender';
    const username = email;
    const DateOfBirth = dateOfBirdthUser.format('DD.MM.YYYY');
    var regData = {
      username,
      password,
      email,
      firstName,
      lastName,
      gender,
      DateOfBirth,
      preferences,
      location
    };
    var result = await IdentityService.registration(regData);
    if(result.status === `ok`)
    {
      window.location.href = 'login';
    }
    else
    {
      setDateOfBirdthUserError(false);
      setEmailError(false);
      setPreferencesError(false);
      setLocationError(false);
      setLastNameError(false);
      setFirstNameError(false);
      setDateOfBirdthUserErrorText('');
      setEmailErrorText('');
      setPreferencesErrorText('');
      setLocationErrorText('');
      setLastNameErrorText('');
      setFirstNameErrorText('');

      var errors = result.error;
      if(errors !== undefined)
      {
        errors.forEach((pr, index) => {
          if(pr.propertyName === 'DateOfBirth')
          {
            setDateOfBirdthUserError(true);
            setDateOfBirdthUserErrorText(pr.errorMessage);
          }
          if(pr.propertyName === 'Email')
          {
            setEmailError(true);
            setEmailErrorText(pr.errorMessage);
          }
          if(pr.propertyName === 'Preferences')
          {
            setPreferencesError(true);
            setPreferencesErrorText(pr.errorMessage);
          }
          if(pr.propertyName === 'LastName')
          {
            setLastNameError(true);
            setLastNameErrorText(pr.errorMessage);
          }
          if(pr.propertyName === 'FirstName')
          {
            setFirstNameError(true);
            setFirstNameErrorText(pr.errorMessage);
          }
          if(pr.propertyName === 'Location')
          {
            setLocationError(true);
            setLocationErrorText(pr.errorMessage);
          }
        });
      }
      else
      {
        setDateOfBirdthUserError(false);
        setEmailError(false);
        setPreferencesError(false);
        setLocationError(false);
        setLastNameError(false);
        setFirstNameError(false);
      }
    }
  }


  useEffect(() => {
    if(loaded > 0)
    {
      return;
    }
    const f = async()=>{
      var datas = await getPreferences();
      setPreferencesPlatform(datas);
      var datal = await getLocations();
      setLocationPlatform(datal);
      setLoaded(1);
    }
    f();
  },[preferencesPlatform]);

  return (
    <Box>
      <Stack 
        component="form"
        sx={{'& .MuiTextField-root': { m: 1, width: '50ch' },
        '& .MuiButton-root': { m: 1, width: '28ch' }, p:3}}
        noValidate
        autoComplete="off" 
        direction="column"
        justifyContent="center"
        alignItems="center"
        >
          <Typography sx={{p:3}}>
              Для того что бы зарегистрироваться, заполните форму:
          </Typography>
          <TextField required
            helperText={firstNameErrorText}
            error={firstNameError}
            id="outlined-required"
            label="Имя"
            //defaultValue="Иван"
            onChange={e => setFirstName(e.target.value)}
          />
          <TextField required
            helperText={lastNameErrorText}
            error={lastNameError}
            id="outlined-required"
            label="Фамилия"
            //defaultValue="Иванов"
            onChange={e => setLastName(e.target.value)}
          />

          <Box sx={{display: 'flex','& > :not(style)': {m: 1,},}}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker 
                  labelId="demo-simple-select-filled-label1" 
                  value={value} label="Дата вашего рождения" 
                  error={dateOfBirdthUserError}
                  slotProps={{
                    textField: {
                      helperText: dateOfBirdthUserErrorText,
                      error:dateOfBirdthUserError
                    },
                  }}
                  onChange={e => setDateOfBirdthUser(e)}/>
            </LocalizationProvider>
          </Box>
          <TextField required
            id="outlined-required"
            label="Электронная почта"
            //defaultValue="mymail@mail.ru"
            helperText={emailErrorText}
            error={emailError}
            onChange={e => setEmail(e.target.value)}
          /> 
          <TextField id="filled-password-input"
            label="Пароль"
            type="password"
            autoComplete="current-password"
            variant="filled"         
            helperText={passwordConfirmUserErrorText}
            error={passwordConfirmUserError}
            onChange={e => setPassword(e.target.value)}
          /> 
          <TextField id="filled-password-input-confirm"
            label="Подтверждение пароля"
            type="password"
            autoComplete="current-password"
            variant="filled"
            helperText={passwordConfirmUserErrorText}
            error={passwordConfirmUserError}
            onChange={e => setPasswordConfirm(e.target.value)}
          /> 
          <Autocomplete required
            multiple
            id="size-small-standard-multi"
            size="small"
            options={preferencesPlatform}
            getOptionLabel={(option) => option.name}
            error={preferencesError}
            onChange={(value, item) => { setPreferences(item.map((i) => i.slug))}}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Выберите ваши интересы"
                placeholder="Интересы"
                helperText={preferencesErrorText}
                error={preferencesError}
              />
            )}
          />
          <Autocomplete required
            id="size-small-standard-multi"
            size="small"
            options={locationPlatform}
            getOptionLabel={(option) => option.name}
            error={locationError}
            onChange={(value, item) => { setLocation(item.slug)}}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Выберите Город"
                placeholder="город"
                helperText={locationErrorText}
                error={locationError}
              />
            )}
          />
          <Box>
            <Button variant="contained" color="primary" type="submit" onClick={handleSubmit}>Регистрация</Button>
            <Button variant="contained" color="primary" type="submit">Отмена</Button>
          </Box>
      </Stack>
    </Box>
  );
}