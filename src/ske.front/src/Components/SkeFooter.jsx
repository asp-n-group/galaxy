import * as React from 'react';
import AspectRatio from '@mui/joy/AspectRatio';
import Box from '@mui/joy/Box';
import Button from '@mui/joy/Button';
import IconButton from '@mui/joy/IconButton';
import Card from '@mui/joy/Card';
import CardContent from '@mui/joy/CardContent';
import Chip from '@mui/joy/Chip';
import Divider from '@mui/joy/Divider';
import Input from '@mui/joy/Input';
import List from '@mui/joy/List';
import ListSubheader from '@mui/joy/ListSubheader';
import ListItem from '@mui/joy/ListItem';
import ListItemDecorator from '@mui/joy/ListItemDecorator';
import ListItemButton from '@mui/joy/ListItemButton';
import Typography from '@mui/joy/Typography';
import Sheet from '@mui/joy/Sheet';
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import GitHubIcon from '@mui/icons-material/GitHub';
import SendIcon from '@mui/icons-material/Send';
import { FormLabel, InputLabel, Link } from '@mui/material';

export default function SkeFooter() {
  const [color, setColor] = React.useState('primary');
  return (
    <Sheet
    position="sticky"
      variant="solid"
      color='primary'
      invertedColors
      sx={{ 
        flexGrow: 1,
        p: 2,
        borderRadius: { xs: 0, sm: 'xs' },
      }}
      
    >
      <Box position="sticky" sx={{ display: 'flex', alignItems: 'center', gap: 2, pb: 3}}>
    
    
      <Box sx={{color:'white','& .MuiLink-root': { color:'white', pl:1, pr:1}}}>
      <Link href="#" underline="hover">
        Лицензия
      </Link>
      <Link href="#" underline="hover">
        Благодарности
      </Link>
      <Link href="#" underline="hover">
        Пользовательское соглашение
      </Link>
      </Box>

         <Box sx={{ ml: 'auto', display: { xs: 'none', md: 'flex' ,alignItems: 'center'} }}>
         <InputLabel id='email-form-label' sx={{color:'white', pr:2}}  >
            Пригласить друга:   
          </InputLabel>
         <Input
          aria-labelledby="email-form-label"
          id="mail-friend"
          variant="soft"
          placeholder="email друга"
          type="email"
          name="email"
          endDecorator={
            <Button variant="soft" aria-label="subscribe">
              <SendIcon />
            </Button>
          } 
        />
         </Box>

      </Box> 
    </Sheet>
  );
}