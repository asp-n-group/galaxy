﻿using SKE.KudaGo.Domain.Model;

namespace SKE.KudaGo.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с событиями
    /// </summary>
    public interface IEventRepository : IRepository<KudaGoResponse<Event>>
    {
        /// <summary>
        /// Запросить все сущности 
        /// </summary>
        /// <returns> массив сущностей</returns>
        IAsyncEnumerable<IEnumerable<Event>> GetAllAsync();
    }
}