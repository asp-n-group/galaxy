﻿using AutoMapper;
using EF = SKE.EventManager.Domain;
using KG = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности изображения.
    /// </summary>
    public class ImageMappings : Profile
    {
        public ImageMappings()
        {
            CreateMap<KG.Image, EF.Image>();
        }
    }
}
