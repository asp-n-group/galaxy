﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SKE.KudaGo.Services.Repositories.Abstractions;

namespace SKE.KudaGo.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий чтения
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public class Repository<T> : IRepository<T> where T : class
    {
        private HttpClient _httpClient;
        private string _url = "https://kudago.com/public-api/v1.4/";

        public async Task<T> GetAllQueryAsync(string path)
        {
            T result;
            using (_httpClient = new())
            {
                using (HttpResponseMessage response = await _httpClient.GetAsync(_url + path))
                {
                    var responseBody = await response.Content.ReadAsStreamAsync();
                    using (Stream stream = responseBody)
                    {
                        StreamReader reader = new StreamReader(stream);
                        result = JsonConvert.DeserializeObject<T>(reader.ReadToEnd(), new IsoDateTimeConverter { DateTimeFormat = "dd.MM.yyyy" });
                    }
                    responseBody.Close();
                }
            }
            return result;

        }
    }
}