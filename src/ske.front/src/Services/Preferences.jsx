import Constants from '../Constants';

export default async function getPreferences()
{
    var url = `${Constants.GATEWAY_HOST}/EventManagerService/Dictionary/GetEventCategories`;
    var datas = await fetch(url,{
        method: 'GET',
      })
      .then(data => data.json());
      if(datas === undefined)
      {
        return {datas:[] };
      }
      return datas.data;
}