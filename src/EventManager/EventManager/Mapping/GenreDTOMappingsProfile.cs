﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class GenreDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public GenreDTOMappingsProfile()
        {
            CreateMap<GenreDTO, EF.Genre>();
            CreateMap<EF.Genre, GenreDTO>();

            CreateMap<EF.Genre, MovieGenreGRPC>()
                .ForMember(x => x.Id, map => map.MapFrom(s => s.Id.ToString()));
        }
    }
}
