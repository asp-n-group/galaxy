﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Implementations;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace Test.DictionaryControllerTest
{
    public class TagTest
    {
        private readonly Mock<ITagRepository> _repoMock;
        private readonly IDictService _service;

        public TagTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _repoMock = fixture.Freeze<Mock<ITagRepository>>();
            _service = fixture.Build<DictService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetTagAsyncTests_CheckResult_ReturnsEnumerableResult()
        {
            // Arrange
            var data = CreateData();
            _repoMock.Setup(repo => repo.GetFitleredAsync()).ReturnsAsync(data);

            // Act
            var result = await _service.GetTags();

            // Assert
            result.Should().BeAssignableTo<List<Tag>>();
            result.Count().Should().Be(data.Count);
        }

        private List<Tag> CreateData()
        {
            return new List<Tag> {
                new Tag() {
                    Id = Guid.Parse("5d314822-56c8-4d0e-a701-cf9f26c4481d"),
                    Name = "музеи и галереи"
                },
                new Tag() {
                    Id = Guid.Parse("c53ad867-6aac-49a1-95c6-596f87761288"),
                    Name = "6+"
                }
            };
        }
    }
}
