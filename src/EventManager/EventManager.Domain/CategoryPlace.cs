﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Категории мест
    /// </summary>
    public class CategoryPlace : BaseEntityExtension
    {
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// связь категорий с местами
        /// </summary>
        public ICollection<CategoryToPlace> CategoryToPlaces { get; set; }
        /// <summary>
        /// места
        /// </summary>
        public ICollection<Place> Places { get; set; }
    }
}
