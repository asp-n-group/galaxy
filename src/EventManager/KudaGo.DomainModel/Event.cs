﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class Event
    {
        private long _dateUnix;
        private DateTime _date;

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("publication_date")]
        public long PublicationDateUnix
        {
            get { return _dateUnix; }
            set
            {
                _dateUnix = value;
                _date = new DateTime(1970, 1, 1)
                    .AddSeconds(_dateUnix);
            }
        }
        [JsonIgnore]
        public DateTime PublicationDate { get { return _date; } set { _date = value; } }
        [JsonProperty("dates")]
        public List<DatePeriod> Dates { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }

        //TODO проверка на объект???
        [JsonProperty("place")]
        public Place? Place { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("body_text")]
        public string BodyText { get; set; }
        [JsonProperty("location")]
        public City Location { get; set; }
        [JsonProperty("categories")]
        public List<string> Categories { get; set; }
        [JsonProperty("tagline")]
        public string TagLine { get; set; }
        [JsonProperty("age_restriction")]
        public string AgeRestriction { get; set; }
        [JsonProperty("price")]
        public string Price { get; set; }
        [JsonProperty("is_free")]
        public bool IsFree { get; set; }
        [JsonProperty("images")]
        public List<Image> Images { get; set; }
        [JsonProperty("favorites_count")]
        public int FavoritesCount { get; set; }
        [JsonProperty("comments_count")]
        public int CommentsCount { get; set; }
        [JsonProperty("site_url")]
        public string SiteUrl { get; set; }
        [JsonProperty("short_title")]
        public string ShortTitle { get; set; }
        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        public Event()
        {
            Dates = new();
            Tags = new();
        }
    }
}
