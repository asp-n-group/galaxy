﻿using Microsoft.OpenApi.Models;
using SKE.EmailNotificator.Service;
using SKE.EmailNotificator.Services.Implementations;
using SKE.EmailNotifiicator.Services.Repositories.Abstractions;
using System.Reflection;

namespace SKE.EmailNotificator
{
    /// <summary>
    /// расширение служб
    /// </summary>
    public static class RegistrationServices
    {
        /// <summary>
        /// добавление служб
        /// </summary>
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IEmailSendingService, EmailSendingService>();
            services.AddHostedService<EmailNotificatorQueueConsumerService>();
            return services;
        }
    }
}