﻿using AutoMapper;
using EMD = SKE.EventManager.Domain;
using KGD = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности жанр фильма.
    /// </summary>
    public class GenreMappings : Profile
    {
        public GenreMappings()
        {
            CreateMap<KGD.Genre, EMD.Genre>()
                .ForMember(d => d.ExternalId, map => map.MapFrom(s => s.Id))
                .ForMember(d => d.Id, map => map.Ignore());
        }
    }
}
