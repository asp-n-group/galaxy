﻿namespace SKE.Core.DTO.EventManager
{
    /// <summary>
    /// Общая часть для всех ответов 
    /// </summary>
    public class IntegrationResponseDTO<T>
    {
        /// <summary>
        /// статус выполнения запроса
        /// </summary>
        /// <returns>200/201 - true; 400/500 - false</returns> 
        public string Status { get; set; }
        /// <summary>
        /// результирующая коллекция, запрашиваемая уу соответствующего контроллера
        /// </summary>
        public List<T> Data { get; set; }
    }
}
