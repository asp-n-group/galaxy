﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;

namespace Identity.Models
{
    public class AppUser : IdentityUser, INotifyPropertyChanged, INotifyPropertyChanging
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        public event PropertyChangingEventHandler? PropertyChanging;

        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnPropertyChanging([CallerMemberName] string? propertyName = null)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }

        private string? firstName;
        public virtual string? FirstName
        {
            get => firstName;
            set
            {
                if (value != firstName)
                {
                    OnPropertyChanging();
                    firstName = value;
                    OnPropertyChanged();
                }
            }
        }

        private string? lastName;
        public virtual string? LastName
        {
            get => lastName;
            set
            {
                if (value != lastName)
                {
                    OnPropertyChanging();
                    lastName = value;
                    OnPropertyChanged();
                }
            }
        }

        private string? gender;
        public virtual string? Gender
        {
            get => gender;
            set
            {
                if (value != gender)
                {
                    OnPropertyChanging();
                    gender = value;
                    OnPropertyChanged();
                }
            }
        }

        private string? location;
        public virtual string? Location
        {
            get => location;
            set
            {
                if (value != location)
                {
                    OnPropertyChanging();
                    location = value;
                    OnPropertyChanged();
                }
            }
        }

        private DateTime dateOfBirthParsed;
        public virtual DateTime DateOfBirthParsed
        {
            get => dateOfBirthParsed;
            set
            {
                if (value != dateOfBirthParsed)
                {
                    OnPropertyChanging();
                    dateOfBirthParsed = value;
                    OnPropertyChanged();
                }
            }
        }

        public virtual ICollection<Pref>? Pref { get; set; }
    }
}