﻿using SKE.Core.DTO.EventManager;

namespace SKE.Feed.Domain
{
    /// <summary>
    /// Модель события ленты
    /// </summary>
    public class FeedEvent
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// дата публикации
        /// </summary>
        public DateTime PublicationDate { get; set; }

        /// <summary>
        /// название
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }

        /// <summary>
        /// описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// полное описание
        /// </summary>
        public string BodyText { get; set; }

        /// <summary>
        /// тэглайн
        /// </summary>
        public string TagLine { get; set; }

        /// <summary>
        /// возрастное ограничение
        /// </summary>
        public string AgeRestriction { get; set; }

        /// <summary>
        /// стоимость
        /// </summary>
        public string Price { get; set; }

        /// <summary>
        /// бесплатное ли событие
        /// </summary>
        public bool IsFree { get; set; }

        /// <summary>
        /// сколько пользователей добавило событие в избранное
        /// </summary>
        public int FavoritesCount { get; set; }

        /// <summary>
        /// короткое название
        /// </summary>
        public string ShortTitle { get; set; }

        /// <summary>
        /// город проведения
        /// </summary>
        public CityDTO Location { get; set; }

        /// <summary>
        /// место проведения
        /// </summary>
        public PlaceDTO Place { get; set; }

        /// <summary>
        /// даты проведения
        /// </summary>
        public ICollection<DatePeriodDTO> Dates { get; set; }

        /// <summary>
        /// картинки
        /// </summary>
        public ICollection<ImageDTO> Images { get; set; }

        /// <summary>
        /// список категорий
        /// </summary>
        public ICollection<CategoryEventDTO> Categories { get; set; }
    }
}
