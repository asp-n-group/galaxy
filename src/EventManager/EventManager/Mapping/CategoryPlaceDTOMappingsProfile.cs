﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности категорий мест.
    /// </summary>
    public class CategoryPlaceDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public CategoryPlaceDTOMappingsProfile()
        {
            CreateMap<CategoryPlaceDTO, EF.CategoryPlace>();
            CreateMap<EF.CategoryPlace, CategoryPlaceDTO>();

            CreateMap<EF.CategoryPlace, CategoryPlaceGRPC>()
                .ForMember(x => x.Id, map => map.MapFrom(s => s.Id.ToString()));
        }
    }
}
