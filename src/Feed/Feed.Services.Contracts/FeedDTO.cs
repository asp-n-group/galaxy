﻿namespace SKE.Feed.Services.Contracts
{
    /// <summary>
    /// Лента
    /// </summary>
    public class FeedDTO
    {
        /// <summary>
        /// События ленты
        /// </summary>
        public List<FeedEventDTO> Events { get; set; }
    }
}
