﻿namespace SKE.EventManager.Services.Contracts
{
    /// <summary>
    /// DTO Фильтра событий
    /// </summary>
    public class EventFilterDTO
    {
        public bool? IncludeCategories { get; set; } = false;
        public bool? IncludeTags { get; set; } = false;
        public bool? IncludeDates { get; set; } = false;
        public bool? IncludePlace { get; set; } = false;
        public bool? IncludeImages { get; set; } = false;
        public bool? IncludeLocation { get; set; } = false;

        public List<Guid> ? EventsId { get; set; } = null;
        public string? CityId { get; set; } = null;
        public DateTime? DateTime { get; set; } = null;
        public List<string>? Tags { get; set; } = null;
        public List<string>? Categories { get; set; } = null;
        public Guid? PlaceId { get; set; } = null;

        public int? PageSize { get; set; } = 100;
        public int? Page { get; set; } = 1;
    }
}