import UseToken from '../UseToken';
import jwt_decode from 'jwt-decode';
 

 
export default { 
    IsAuthorized: () => {
        var token = UseToken().token;
        return !(token === undefined);
    },
    decodeJWT: (token) => {
        const decoded = jwt_decode(token);
        return decoded;
    },
    getToken:() =>
    {
        const userToken = JSON.parse(sessionStorage.getItem('token'));
        var token = userToken?.token;
        return token;
    },
    saveUserId: function (id) 
    {
        sessionStorage.setItem('userId', id);
    },
    getUserId:() =>
    {
        return sessionStorage.getItem('userId');
    },
    saveEmail:function (email) 
    {
        sessionStorage.setItem('email', email);
    },
    getEmail:() =>
    {
        return sessionStorage.getItem('email');
    },
    clearSession:()=>
    {
        sessionStorage.clear();
    },
    setSessionVars:function(token){
        const decoded = jwt_decode(token);
        sessionStorage.setItem('token', token);
        this.saveUserId(decoded.sub);
        this.saveEmail(decoded.email);
        return decoded;
    }
}