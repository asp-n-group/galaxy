﻿using SKE.Activity.Services.Contracts;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SKE.Activity.Domain;
using SKE.Activity.Services.Repositories.Abstractions;
using Microsoft.AspNetCore.Authorization;

namespace SKE.Activity.Controllers
{
    /// <summary>
    /// лайки
    /// </summary>
    [Authorize()]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class LikesController
        : ControllerBase
    {
        private readonly IRepository<Likes> _likesRepository;
        private IMapper _mapper;

        /// <summary>
        /// ctor лайки.
        /// </summary>
        public LikesController(IRepository<Likes> likesRepository, IMapper mapper)
        {
            _likesRepository = likesRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные всех лайков по событию
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<LikeDTO>> GetLikesByEventAsync(Guid eventId)
        {
            var comments = await _likesRepository
                .GetWhere(x => x.EventId == eventId);
            return _mapper.Map<List<Likes>, List<LikeDTO>>(comments.ToList()).OrderBy(x => x.PublicationDatetime).ToList();
        }

        /// <summary>
        /// Получить данные по пользователю и событию
        /// </summary>
        /// <param name="eventId">Id события, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="userId">Id сотрудника, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpGet("getLikeByEventUser")]
        public async Task<bool> GetLikeByEventUser([FromQuery] LikesModel model)
        {
            var hasLike = (await _likesRepository
                .GetWhere(x => x.EventId == model.eventId && x.UserId == model.userId)).FirstOrDefault();
            return hasLike is not null ? hasLike.isLike : false;
        }

        /// <summary>
        /// добавить лайк
        /// </summary>
        /// <param name="eventId">Id события, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="userId">Id сотрудника, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpPost("AddLike")]
        public async Task<IActionResult> AddLikeAsync([FromQuery] LikesModel model)
        {
            var commentModel = new Likes()
            {
                EventId = model.eventId,
                UserId = model.userId,
                isLike = true,
                PublicationDatetime = DateTime.Now
            };
            var id = await _likesRepository.AddAsync(commentModel);
            return Ok(id);
        }

        /// <summary>
        /// Удалить лайк
        /// </summary>
        /// <param name="eventId">Id события, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="userId">Id сотрудника, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpDelete("DeleteLike")]
        public async Task<IActionResult> DeleteLikeAsync([FromQuery] LikesModel model)
        {
            var like = (await _likesRepository
                .GetWhere(x => x.EventId == model.eventId && x.UserId == model.userId && x.isLike)).FirstOrDefault();

            if (like == null)
                return NotFound();

            await _likesRepository.DeleteAsync(like);

            return Ok();
        }
    }
}