import { useState } from 'react';

export default function UseToken() {
 
    const tokenString = sessionStorage.getItem('token');
    const userToken = JSON.parse(tokenString);
    var token = userToken?.token;
 

 
 
/*
  fetch('http://localhost:60008/api/Auth/is-authorized', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  })
  .then(data => data);
 */
   

  return { 
    token
  }
}
