﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Связь многие ко многим категории мест и места
    /// </summary>
    public class CategoryToPlace : BaseEntity
    {
        /// <summary>
        /// идентификатор категории места
        /// </summary>
        public Guid CategoryPlaceId { get; set; }
        /// <summary>
        /// категория места
        /// </summary>
        public CategoryPlace CategoryPlace { get; set; }

        /// <summary>
        /// идентификатор места
        /// </summary>
        public Guid PlaceId { get; set; }
        /// <summary>
        /// место
        /// </summary>
        public Place Place { get; set; }
    }
}
