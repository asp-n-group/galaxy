﻿using SKE.KudaGo.Domain.Model;
using SKE.KudaGo.Services.Repositories.Abstractions;

namespace SKE.KudaGo.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с категориями мест
    /// </summary>
    public class CategoryPlaceRepository : Repository<IEnumerable<CategoryPlace>>, ICategoryPlaceRepository
    {
        private readonly string _urlPath = "place-categories/?order_by=id&fields=id,slug,name";

        /// <summary>
        /// Получить список категорий мест
        /// </summary>
        /// <returns>список категорий мест</returns>
        public async Task<IEnumerable<CategoryPlace>> GetAllAsync()
        {
            return await GetAllQueryAsync(_urlPath);
        }
    }
}
