﻿
using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Изображения
    /// </summary>
    public class Image : BaseEntity
    {
        /// <summary>
        /// ссылка на изображение 
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// миниатюры 
        /// </summary>
        public Thumbnail? Thumbnails { get; set; }
        /// <summary>
        /// источник
        /// </summary>
        public Source Source { get; set; }

        /// <summary>
        /// идентификатор события 
        /// </summary>
        public Guid? EventId { get; set; }
        /// <summary>
        /// событие 
        /// </summary>
        public Event? Event { get; set; }
        /// <summary>
        /// идентификатор места 
        /// </summary>
        public Guid? PlaceId { get; set; }
        /// <summary>
        /// место 
        /// </summary>
        public Place? Place { get; set; }
        /// <summary>
        /// идентификатор фильма 
        /// </summary>
        public Guid? MovieId { get; set; }
        /// <summary>
        /// фильм 
        /// </summary>
        public Movie? Movie { get; set; }
        /// <summary>
        /// идентификатор постера фильма
        /// </summary>
        public Guid? MoviePosterId { get; set; }
        /// <summary>
        /// постера фильма 
        /// </summary>
        public Movie? MoviePoster { get; set; }
    }
}
