import * as React from 'react';
import { useEffect } from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import StartsRating from './StarsRating';
import Like from './Like';
import Carusel from './Carusel';
import Divider from '@mui/material/Divider';
import Paper from '@mui/material/Paper';
import CardOverflow from '@mui/joy/CardOverflow';
import Table from '@mui/joy/Table';
import Sheet from '@mui/joy/Sheet';
import UseToken from '../UseToken';
import UserService from '../Services/UserService';
import Constants from '../Constants';
import CircleService from '../Services/CircleService';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import AddEventToCircle from '../Components/AddEventToCircle';
import Grid from '@mui/material/Unstable_Grid2';
import CommentBox from '../Components/CommentBox';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => { 
    const { expand, ...other } = props;
    return <IconButton {...other} />; 
  })
  (({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  }));

function prepareTable(p)
{ 
  var row1 = 
  {
    name1:'Мероприятие:', 
    value1:p.itemDetails?.shortTitle,
    name2:'Стоимость:', 
    value2:p.itemDetails?.price
  }; 
  var row2 = 
  {
    name1:'Возраст:', 
    value1:p.itemDetails?.ageRestriction,
    name2:'Город:', 
    value2: p.itemDetails?.location?.name ?? p.itemDetails?.place?.location?.name    
  }; 
  var row3 = 
  {
    name1:'Название:', 
    value1:p.itemDetails?.place?.title,
    name2:'Адрес:',
    value2:p.itemDetails?.place?.address,
  };
  var row4 = 
  {
    name1:'Касса:', 
    value1:p.itemDetails?.place?.timeTable,
    name2:'Телефон кассы:', 
    value2:p.itemDetails?.place?.phone
  };
  var o = {};
  var key = 'additional_data';
  o[key] = [];
  o[key].push(row1);
  o[key].push(row2);
  o[key].push(row3);
  o[key].push(row4);
  return o;
}

// посмотреть есть ли SSL HTTPS в запущеном докере
export default function EventItemDetailsCard(props) {
  const [expanded, setExpanded] = React.useState(true);
  const [circles, setCircles] = React.useState([{users:[]}]);
  const [loaded, setLoaded] = React.useState(false);
  const [age, setCircle] = React.useState('');

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const handleClickLukas = async () => {
    
    var token = UseToken().token;
      console.log(UserService.decodeJWT(token));
      /*
      var userId = UserService.decodeJWT(token).sub;
      var datas = await fetch(`${Constants.HOST}:60006/api/v1/Circle?userId=${userId}&name=${steps[0].id}`,{
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
          }
        })
        .then(data => data.json());
        if(datas === undefined)
        {
          return {events:[] };
        }
      */
  };  

  useEffect(() => {
    if(loaded)
    {
      return;
    }
    setLoaded(true);
    const datap = async ()=>{
      var d = await CircleService.getUserCircles();
      setCircles(d);
    };
    datap();
  },[circles, loaded]);

  const ShowComments = () => {
    if (props.selectCircle !== null) {
      return <> 
        <CommentBox selectEvent={props.itemDetails} selectCircle={props.selectCircle}/>
      </>;
    }
  }

  var i=1;
  var rows = prepareTable(props);
   
  return (
    
    <Box>   
      <Card sx={{ display: 'flex', p:1 }}>
        <CardMedia sx={{ display: 'flex', p:6}}>
          <Box sx={{ width:400, height:400, flexGrow: 1 }}> 
            <Carusel images={props.itemDetails?.images ?? []} /> 
            <AddEventToCircle itemDetails={props.itemDetails}/>
          </Box>
        </CardMedia>         
        <Box color="text.secondary">
          <CardContent>
            <Typography sx={{b:1}} variant="h5">
              <div dangerouslySetInnerHTML={{__html: props.itemDetails?.description}} />
            </Typography>
            <Typography paragraph>
              <div dangerouslySetInnerHTML={{__html: props.itemDetails?.bodyText}} />
            </Typography>
          </CardContent>
          <Divider variant="middle" />
          <CardActions disableSpacing sx={{ position: 'relative', bottom: 0, left: 0}} >
            <Like itemDetails={props.itemDetails}/>
            <StartsRating/>
          </CardActions>
          <Divider variant="middle" />
          <CardActions disableSpacing sx={{ position: 'relative', bottom: 0, left: 0}} >
            <Box sx={{ width: '100%'}}>
              {rows.additional_data.map((row) => (
                <Grid key={i++} container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                  <Grid key={row.name1} xs={3}><b>{row.name1}</b></Grid>
                  <Grid key={row.value1} xs={3}>{row.value1}</Grid>
                  <Grid key={row.name2} xs={3}><b>{row.name2}</b></Grid>
                  <Grid key={row.value2} xs={3}>{row.value2}</Grid>
                </Grid>
              ))}
            </Box>
          </CardActions>
        </Box>  
      </Card>
      {ShowComments()}
    </Box>
  );
}