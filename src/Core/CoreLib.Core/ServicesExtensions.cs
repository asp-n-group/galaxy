using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace SKE.CoreLib.Core
{
    public static class ServicesExtensions
    {
        public static IServiceCollection AddAuth(this IServiceCollection services, IConfiguration configuration)
            {
                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidIssuer = configuration["Jwt:Issuer"],
                            ValidateAudience = true,
                            ValidAudience = configuration["Jwt:Audience"],
                            ValidateLifetime = true,
                            IssuerSigningKey = new SymmetricSecurityKey(
                                Encoding.UTF8.GetBytes(configuration["Jwt:Key"])),
                            ValidateIssuerSigningKey = true,
                        };
                    });
                services.AddAuthorization();
                return services;
            }

        public static IServiceCollection AddCorsReg(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddCors(options => {
                options.AddDefaultPolicy(
                    policy => {
                        policy.WithOrigins(configuration["Front"].Split(','))
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
            });
        } 
    }
}

