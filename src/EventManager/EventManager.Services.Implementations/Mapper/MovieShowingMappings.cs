﻿using AutoMapper;
using EMD = SKE.EventManager.Domain;
using KGD = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности показы Фильма.
    /// </summary>
    public class MovieShowingMappings : Profile
    {
        public MovieShowingMappings()
        {
            CreateMap<KGD.MovieShowings, EMD.MovieShowings>()
                .ForMember(d => d.ExternalId, map => map.MapFrom(s => s.Id))
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.MovieId, map => map.Ignore())
                .ForMember(d => d.PlaceId, map => map.Ignore())
                .ForMember(d => d.Movie, map => map.Ignore())
                .ForMember(d => d.Place, map => map.Ignore());
        }
    }
}
