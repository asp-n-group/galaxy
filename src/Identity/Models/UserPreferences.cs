﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Identity.Models
{
    public class UserPreference : INotifyPropertyChanged, INotifyPropertyChanging
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        public event PropertyChangingEventHandler? PropertyChanging;

        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnPropertyChanging([CallerMemberName] string? propertyName = null)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }

        private string? userId;
        public virtual string? UserId
        {
            get => userId;
            set
            {
                if (value != userId)
                {
                    OnPropertyChanging();
                    userId = value;
                    OnPropertyChanged();
                }
            }
        }

        private int preferenceId;
        public virtual int PreferenceId
        {
            get => preferenceId;
            set
            {
                if (value != preferenceId)
                {
                    OnPropertyChanging();
                    preferenceId = value;
                    OnPropertyChanged();
                }
            }
        }

        private AppUser? user;
        public virtual AppUser? User
        {
            get => user;
            set
            {
                if (value != user)
                {
                    OnPropertyChanging();
                    user = value;
                    OnPropertyChanged();
                }
            }
        }

        private Preference? preference;
        public virtual Preference? Preference
        {
            get => preference;
            set
            {
                if (value != preference)
                {
                    OnPropertyChanging();
                    preference = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
