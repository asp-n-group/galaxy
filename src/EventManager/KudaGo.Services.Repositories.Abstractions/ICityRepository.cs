﻿using SKE.KudaGo.Domain.Model;

namespace SKE.KudaGo.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с городами
    /// </summary>
    public interface ICityRepository : IRepository<IEnumerable<City>>
    {
        /// <summary>
        /// Запросить все сущности 
        /// </summary>
        /// <returns> массив сущностей</returns>
        Task<IEnumerable<City>> GetAllAsync();
    }
}