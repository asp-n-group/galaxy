﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Identity.Models
{
    public class Pref : INotifyPropertyChanged, INotifyPropertyChanging
    {
        [Key]
        public virtual int Id { get; set; }

        public event PropertyChangedEventHandler? PropertyChanged;
        public event PropertyChangingEventHandler? PropertyChanging;

        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnPropertyChanging([CallerMemberName] string? propertyName = null)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }

        private string? userId;
        public virtual string? UserId
        {
            get => userId;
            set
            {
                if (value != userId)
                {
                    OnPropertyChanging();
                    userId = value;
                    OnPropertyChanged();
                }
            }
        }

        private string? preference;
        public virtual string? Preference
        {
            get => preference;
            set
            {
                if (value != preference)
                {
                    OnPropertyChanging();
                    preference = value;
                    OnPropertyChanged();
                }
            }
        }

        private AppUser? user;
        public virtual AppUser? User
        {
            get => user;
            set
            {
                if (value != user)
                {
                    OnPropertyChanging();
                    user = value;
                    OnPropertyChanged();
                }
            }
        }


    }
}
