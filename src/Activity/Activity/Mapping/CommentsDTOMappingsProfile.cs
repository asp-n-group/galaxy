﻿using SKE.Activity.Services.Contracts;
using AutoMapper;
using SKE.Activity.Domain;

namespace SKE.Activity.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности комментария.
    /// </summary>
    public class CommentsDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor автомаппера для сущности комментария.
        /// </summary>
        public CommentsDTOMappingsProfile()
        {
            CreateMap<Comment, CommentDTO>();
            CreateMap<CommentDTO, Comment>();
        }
    }
}
