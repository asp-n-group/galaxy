﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Изображения в меньшем разрешении
    /// </summary>
    public class Thumbnail : BaseEntity
    {
        /// <summary>
        /// ссылка на миниатюру 640*384
        /// </summary>
        public string Url640x384 { get; set; }
        /// <summary>
        /// ссылка на миниатюру 144*96
        /// </summary>
        public string Url144x96 { get; set; }

        /// <summary>
        /// идентификатор изображения
        /// </summary>
        public Guid? ImageId { get; set; }
        /// <summary>
        /// Изображение
        /// </summary>
        public Image? Image { get; set; }
    }
}
