﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class PlaceDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public PlaceDTOMappingsProfile()
        {
            CreateMap<PlaceDTO, EF.Place>();
            CreateMap<EF.Place, PlaceDTO>();

            CreateMap<EF.Place, PlaceGRPC>();
        }
    }
}
