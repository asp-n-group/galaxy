using System.ComponentModel.Design;
using SKE.Feed;
using System.Net;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using SKE.CoreLib.Core;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services
    //.AddDbContext<UserActivityDataContext>(options =>
    //    options.UseNpgsql("Host=localhost;port=5432;Username=postgres;Password=postgres;Database=SidekicksEventsUserActivity"))
    .AddServices(builder.Configuration)
    .AddAutomapper()
    .AddEndpointsApiExplorer()
    .AddSwaggerGen()
    .AddCorsReg(builder.Configuration)
    .AddAuth(builder.Configuration)
    .AddControllers();

ServicePointManager.ServerCertificateValidationCallback +=
    (sender, cert, chain, sslPolicyErrors) => true;

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();
app.UseRouting();
app.UseCors(); 
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.Run();
