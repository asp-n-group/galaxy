﻿using SKE.Activity.Services.Contracts;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SKE.Activity.Domain;
using SKE.Activity.Services.Repositories.Abstractions;
using Microsoft.AspNetCore.Authorization;

namespace SKE.Activity.Controllers
{
    /// <summary>
    /// комментарии
    /// </summary>
    [Authorize()]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CommentsController
        : ControllerBase
    {
        private readonly IRepository<Comment> _commentRepository;
        private IMapper _mapper;

        /// <summary>
        /// Контроллер комментариев
        /// </summary>
        public CommentsController(IRepository<Comment> commentRepository, IMapper mapper)
        {
            _commentRepository = commentRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные всех комментариев по кругу и событию
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetComments")]
        public async Task<List<CommentDTO>> GetCommentsByCirleEventAsync(Guid circeId, Guid eventId)
        {
            var comments = await _commentRepository
                .GetWhere(x => x.EventId == eventId && x.CircleId == circeId);
            return _mapper.Map<List<Comment>, List<CommentDTO>>(comments.ToList()).OrderBy(x => x.PublicationDatetime).ToList();

        }

        /// <summary>
        /// добавить комментарий
        /// </summary>
        /// <param name="circeId">Id круга, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="eventId">Id события, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="userId">Id сотрудника, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="message">сообщение, например <example>simple text</example></param>
        /// <returns></returns>
        [HttpPost("AddComment")]
        public async Task<IActionResult> AddCommentAsync(CommentModel model)
        {
            var commentModel = new Comment()
            {
                CircleId = model.circeId,
                EventId = model.eventId,
                UserId = model.userId,
                Message = model.message,
                PublicationDatetime = DateTime.Now,
                UserName = model.userName,
            };
            await _commentRepository.AddAsync(commentModel);

            return Ok();
        }

        /// <summary>
        /// Обновить комментарий
        /// </summary>
        /// <param name="id">Id комментария, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="message">текст сообщениия</param>
        /// <returns></returns>
        [HttpPut("UpdateComment")]
        public async Task<IActionResult> UpdateCommentAsync(Guid id, string message)
        {
            var comment = await _commentRepository.GetByIdAsync(id);

            if (comment == null)
                return NotFound();

            comment.Message = message;

            await _commentRepository.UpdateAsync(comment);

            return Ok();
        }

        /// <summary>
        /// Удалить комментарий
        /// </summary>
        /// <param name="id">Id комментария, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpDelete("RemoveComment")]
        public async Task<IActionResult> DeleteCommentAsync(Guid id)
        {
            var comment = await _commentRepository.GetByIdAsync(id);

            if (comment == null)
                return NotFound();

            await _commentRepository.DeleteAsync(comment);

            return Ok();
        }
    }
}