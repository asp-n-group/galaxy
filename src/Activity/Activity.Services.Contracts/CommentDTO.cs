﻿namespace SKE.Activity.Services.Contracts
{
    public class CommentDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// ссылка на пользователя
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// дата время публикации
        /// </summary>
        public DateTime PublicationDatetime { get; set; }
        /// <summary>
        /// имя пользователя
        /// </summary>
        public string UserName { get; set; } = string.Empty;
        /// <summary>
        /// сообщение
        /// </summary>
        public string Message { get; set; } = string.Empty;
    }
}