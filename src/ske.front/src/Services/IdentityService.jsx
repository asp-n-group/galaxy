import Constants from '../Constants';

export default {
    registration : async function(data) 
    {
        var res = { status:``, error:``}
        var result = await fetch(`${Constants.GATEWAY_HOST}/IdentityService/Auth/Register`, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify(data) 
        });
        if(result.ok)
        {
            res.status = `ok`;
        }
        else
        {
            res.status = `error`;
            res.error = await result.json();
        }
        return res;
    },
    update: async function(data) 
    {
        var res = { status:``, error:``}
        var result = await fetch(`${Constants.GATEWAY_HOST}/IdentityService/Auth/Update`, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify(data) 
        });
        if(result.ok)
        {
            res.status = `ok`;
        }
        else
        {
            res.status = `error`;
            res.error = await result.json();
        }
        return res;
    },    
    login: async function(credentials) 
    {
        var result = await fetch(`${Constants.GATEWAY_HOST}/IdentityService/Auth/login`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        })
        .then(data => data.json())
        return result
    },
    userName: async function(id)
    {
        var result = await fetch(`${Constants.GATEWAY_HOST}/IdentityService/Users/userName/${id}`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            }
            })
            .then(data => data.text() );
        return result
    },    
    userInfo: async function(email) 
    {
        return await fetch(`${Constants.GATEWAY_HOST}/IdentityService/Users/login/${email}`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            }
            }).then(data => data.json());
    }
}