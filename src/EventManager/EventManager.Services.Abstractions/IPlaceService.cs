﻿using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Services.Abstractions
{
    public interface IPlaceService
    {
        Task<IEnumerable<Place>> GetPlaces(PlaceFilterDTO placeFilterDTO);
    }
}
